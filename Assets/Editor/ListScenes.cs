using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Example
{
    // agrega un men� item  que ofrece un breve resumen de las escenas abiertas actualmente
    [MenuItem("Escena de ejemplo/Resumen de escena")]
    public static void ListSceneNames()
    {
        string output = "";
        if (SceneManager.sceneCount > 0)
        {
            for (int n = 0; n < SceneManager.sceneCount; ++n)
            {
                Scene scene = SceneManager.GetSceneAt(n);
                output += scene.name;
                output += scene.isLoaded ? " (Cargada, " : " (Sin cargar, ";
                output += scene.isDirty ? "Sucia, " : "Limpia, ";
                output += scene.buildIndex >= 0 ? " En build)\n" : " NO en build)\n";
            }
        }
        else
        {
            output = "Scenes sin abrir.";
        }
        EditorUtility.DisplayDialog("Resumen de escena", output, "Ok");
    }
}