﻿using Actions;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace ActionsTests
{
    public class GetLoggedPlayerTests
    {
        private IUserSessionRepo _userSessionRepo;
        private Player _mockPlayer;
        private UserSession _mockUserSession;
        private PlayerData _mockPlayerData;

        [SetUp]
        public void Setup()
        {
            _mockPlayerData = new PlayerData(1, "Elian", new List<PowerUp>());
            _mockPlayer = new Player("eremaggi", _mockPlayerData);
            _mockUserSession = new UserSession(_mockPlayer);

            _userSessionRepo = Substitute.For<IUserSessionRepo>();
            _userSessionRepo.GetCurrentUserSession().Returns(_mockUserSession);
        }

        [Test]
        public void GetLoggedPlayerShouldReturnLoggedPlayer()
        {
            //Arrange
            //..

            //Act
            
            GetLoggedPlayer action = new GetLoggedPlayer(_userSessionRepo);
            
            //Assert
            Assert.AreEqual(_mockPlayer, action.Execute());
        }
    }
}