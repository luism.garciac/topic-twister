﻿using Actions;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace ActionsTests
{
    public class GetSessionOpponentTests
    {
        private IGameSessionRepo _gameSessionRepo;
        private IUserSessionRepo _userSessionRepo;

        private GameSession _gameSession;

        Player _player;
        Player _opponent;

        [SetUp]
        public void Setup()
        {
            _gameSessionRepo = Substitute.For<IGameSessionRepo>();
            _userSessionRepo = Substitute.For<IUserSessionRepo>();

            _player = new Player("eremaggi", new PlayerData(3, "Elian", new List<PowerUp>()));
            _opponent = new Player("lgarcia", new PlayerData(1, "Luis", new List<PowerUp>()));

            _gameSession = new GameSession(_player, _opponent);

            _userSessionRepo.GetCurrentUserSession().Returns(new UserSession(_player));
            _gameSessionRepo.GetCurrentGameSession().Returns(_gameSession);
        }

        [Test]
        public void GetSessionOpponentShouldReturnCurrentSessionOpponent()
        {
            //Arrange
            var action = new GetSessionOpponent(_gameSessionRepo, _userSessionRepo);
            //Act
            //...

            //Assert
            Assert.AreEqual(_opponent, action.Execute());
        }
    }
}