﻿using Actions;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace ActionsTests
{
    public class GetRoundCategoryNamesTests
    {
        private IGameSessionRepo _sessionRepo;
        private List<Category> _testCategories = new List<Category>();
        private List<string> _testCategoriesStrings = new List<string>();

        private Player _player = new Player("eremaggi", new PlayerData(3, "Elian", new List<PowerUp>()));
        private Player _opponent = new Player("lgarcia", new PlayerData(1, "Luis", new List<PowerUp>()));

        [SetUp]
        public void Setup()
        {
            _sessionRepo = Substitute.For<IGameSessionRepo>();

            Category testCategory = new Category(1, "TestCategory", new List<string>() { "Answer" });

            _testCategories.Add(testCategory);
            _testCategories.Add(testCategory);
            _testCategories.Add(testCategory);
            _testCategories.Add(testCategory);
            _testCategories.Add(testCategory);

            foreach (var category in _testCategories)
            {
                _testCategoriesStrings.Add(category.CategoryName);
            }

            GameSession gameSession = new GameSession(_player, _opponent);
            Round testRound = new Round(_testCategories, 1);
            gameSession.CurrentRound = testRound;

            _sessionRepo.GetCurrentGameSession().Returns(gameSession);
        }

        [Test]
        public void GetRoundCategoryNamesShouldReturnCurrentRoundCategories()
        {
            //Arrange
            List<string> categoryNames = new List<string>();
            GetRoundCategoryNames getRoundCategoryNames = new GetRoundCategoryNames(_sessionRepo);
            //Act

            categoryNames = getRoundCategoryNames.Execute();
            //Assert
            Assert.AreEqual(categoryNames, _testCategoriesStrings);
        }
    }
}