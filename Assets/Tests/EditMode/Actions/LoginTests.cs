﻿using Actions;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace ActionsTests
{
    public class LoginTests
    {
        private IWebLoginService _webLoginService;
        private IUserSessionRepo _userSessionRepository;
        private Player _mockPlayer;
        private PlayerData _mockPlayerData;

        [SetUp]
        public void Setup()
        {
            _mockPlayerData = new PlayerData(1, "Elian", new List<PowerUp>());
            _mockPlayer = new Player("eremaggi", _mockPlayerData);

            _webLoginService = Substitute.For<IWebLoginService>();
            _userSessionRepository = Substitute.For<IUserSessionRepo>();

            _webLoginService.FindPlayerByID("eremaggi").Returns(_mockPlayer);
        }

        [Test]
        public async void IfPlayerLoginSuccessfulReturnTrue()
        {
            //Arrange
            //...

            //Act
            Login login = new Login(_userSessionRepository, _webLoginService);

            //Assert
            Assert.IsTrue(await login.Execute("eremaggi"));
        }

        [Test]
        public async void IfPlayerLoginSuccessCreateSession()
        {
            //Arrange
            //...

            //Act
            Login login = new Login(_userSessionRepository, _webLoginService);
            await login.Execute("eremaggi");

            //Assert
            _userSessionRepository.Received().CreateUserSession(_mockPlayer);
        }
    }
}