﻿using Actions;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace ActionsTests
{
    public class GetPlayerGameSessionsTests
    {
        private IWebGameSessionServices _webSessionsRepo;
        private IUserSessionRepo _userSessionRepo;

        private List<GameSession> _mockGameSessions = new List<GameSession>();

        private Player _player;
        private Player _opponent;

        private GetPlayerSessions _getPlayerGameSessions;

        [SetUp]
        public void Setup()
        {
            _webSessionsRepo = Substitute.For<IWebGameSessionServices>();
            _userSessionRepo = Substitute.For<IUserSessionRepo>();

            _player = new Player("eremaggi", new PlayerData(3, "Elian", new List<PowerUp>()));
            _opponent = new Player("lgarcia", new PlayerData(1, "Luis", new List<PowerUp>()));

            _userSessionRepo.GetCurrentUserSession().Returns(new UserSession(_player));

            GameSession testGameSession = new GameSession(_player, _opponent);

            _mockGameSessions.Add(testGameSession);
            _mockGameSessions.Add(testGameSession);
            _mockGameSessions.Add(testGameSession);

            _webSessionsRepo.FindOpenGameSessions("eremaggi").Returns(_mockGameSessions);

            _getPlayerGameSessions = new GetPlayerSessions(_webSessionsRepo, _userSessionRepo);
        }

        [Test]
        public async void GetPlayerGameSessionsReturnsLoggedPlayerGameSessions()
        {
            //Arrange
            //..

            //Act
            List<GameSession> playerGameSessions = await _getPlayerGameSessions.Execute();

            //Assert
            Assert.AreEqual(_mockGameSessions,playerGameSessions);
        }
     }
}