﻿using Actions;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace ActionsTests
{
    public class FindMatchTest
    {
        private IWebMatchMakingServices _webMatchMakingServices;

        private Player _player = new Player("eremaggi", new PlayerData(3, "Elian", new List<PowerUp>()));
        private Player _opponent = new Player("lgarcia", new PlayerData(1, "Luis", new List<PowerUp>()));

        [SetUp]
        public void Setup()
        {
            _webMatchMakingServices = Substitute.For<IWebMatchMakingServices>();
            _webMatchMakingServices.FindOpponent("eremaggi").Returns(new GameSession(_player, _opponent));

        }
        [Test]
        public async void IfFindMatchIsExecutedReturnNewGameSession()
        {
            //Arrange
            FindMatch findMatch = new FindMatch(_webMatchMakingServices);

            //Act
            var gameSession = await findMatch.Execute(_player);

            //Assert
            Assert.IsNotNull(gameSession);
        }
    }
}