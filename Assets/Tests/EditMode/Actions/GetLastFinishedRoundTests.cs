﻿using Actions;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace ActionsTests
{
    public class GetLastFinishedRoundTests
    {
        private IGameSessionRepo _gameSessionRepo;

        private List<Answer> _testAnswerList = new List<Answer>();
        private List<Category> _testCategories = new List<Category>();
        private Answer _testAnswer;

        private Category _testCategory;
        private Round _testRound;
        private GameSession _gameSession;

        private Player _player;
        private Player _opponent;

        [SetUp]
        public void Setup()
        {
            _gameSessionRepo = Substitute.For<IGameSessionRepo>();

            _player = new Player("eremaggi", new PlayerData(3, "Elian", new List<PowerUp>()));
            _opponent = new Player("lgarcia", new PlayerData(1, "Luis", new List<PowerUp>()));

            _testAnswer = new Answer("TestAnswer", true);
            _testCategory = new Category(1, "TestCategory", new List<string>() { "Answer" });

            _testCategories.Add(_testCategory);
            _testCategories.Add(_testCategory);
            _testCategories.Add(_testCategory);
            _testCategories.Add(_testCategory);
            _testCategories.Add(_testCategory);

            _testAnswerList.Add(_testAnswer);
            _testAnswerList.Add(_testAnswer);
            _testAnswerList.Add(_testAnswer);
            _testAnswerList.Add(_testAnswer);
            _testAnswerList.Add(_testAnswer);

            _gameSession = new GameSession(_player, _opponent);

            _testRound = new Round(_testCategories, 1);
        }

        [Test]
        public void IfBothPlayersAnsweredRoundReturnThisRound()
        {
            //Arrange
            _testRound.Player1Answers = _testAnswerList;
            _testRound.Player2Answers = _testAnswerList;

            _gameSession.MatchRounds.Add(_testRound);

            _gameSessionRepo.GetCurrentGameSession().Returns(_gameSession);

            var action = new GetLastFinishedRound(_gameSessionRepo);

            //Act
            //...

            //Assert
            Assert.AreEqual(_testRound, action.Execute());
        }

        [Test]
        public void IfThereIsNoFinishedRoundReturnNull()
        {
            //Arrange
            _testRound.Player1Answers = _testAnswerList;
            _testRound.Player2Answers = new List<Answer>();

            _gameSession.MatchRounds.Add(_testRound);

            _gameSessionRepo.GetCurrentGameSession().Returns(_gameSession);

            var action = new GetLastFinishedRound(_gameSessionRepo);

            //Act
            //...

            //Assert
            Assert.IsNull(action.Execute());
        }
    }
}