﻿using Actions;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace ActionsTests
{
    public class CheckPlayerTurnTests
    {
        private IGameSessionRepo _gameSessionRepo;
        private IUserSessionRepo _userSessionRepo;

        private List<Answer> _testAnswerList = new List<Answer>();
        private List<Category> _testCategories = new List<Category>();
        private Answer _testAnswer;

        private Category _testCategory;
        private Round _testRound;
        private GameSession _gameSession;

        Player _player;
        Player _opponent;

        [SetUp]
        public void Setup()
        {
            _gameSessionRepo = Substitute.For<IGameSessionRepo>();
            _userSessionRepo = Substitute.For<IUserSessionRepo>();

            _player = new Player("eremaggi", new PlayerData(3, "Elian", new List<PowerUp>()));
            _opponent = new Player("lgarcia", new PlayerData(1, "Luis", new List<PowerUp>()));

            _userSessionRepo.GetCurrentUserSession().Returns(new UserSession(_player));

            _testAnswer = new Answer("TestAnswer", true);
            _testCategory = new Category(1, "TestCategory", new List<string>() { "Answer" });

            _testCategories.Add(_testCategory);
            _testCategories.Add(_testCategory);
            _testCategories.Add(_testCategory);
            _testCategories.Add(_testCategory);
            _testCategories.Add(_testCategory);

            _testAnswerList.Add(_testAnswer);
            _testAnswerList.Add(_testAnswer);
            _testAnswerList.Add(_testAnswer);
            _testAnswerList.Add(_testAnswer);
            _testAnswerList.Add(_testAnswer);

            _gameSession = new GameSession(_player, _opponent);

            _testRound = new Round(_testCategories, 1);           
        }

        [Test]
        public void IfBothPlayerAndOpponentHaveAnsweredThenIsPlayerTurn()
        {
            //Arrange
            _testRound.Player1Answers = _testAnswerList;
            _testRound.Player2Answers = _testAnswerList;
            _gameSession.CurrentRound = _testRound;

            _gameSession.MatchRounds.Add(_testRound);

            _gameSessionRepo.GetGameSessionByID(1,false).Returns(_gameSession);
            _gameSessionRepo.GetCurrentGameSession().Returns(_gameSession);

            var action = new CheckPlayerTurn(_gameSessionRepo, _userSessionRepo);

            //Act
            //...

            //Assert
            Assert.IsTrue(action.Execute(1));
            Assert.IsTrue(action.Execute(_gameSession));
        }

        [Test]
        public void IfOnlyPlayerHasAnsweredThenIsNotPlayerTurn()
        {
            //Arrange
            _testRound.Player1Answers = new List<Answer>();
            _testRound.Player2Answers = _testAnswerList;
            _gameSession.CurrentRound = _testRound;

            _gameSession.MatchRounds.Add(_testRound);

            _gameSessionRepo.GetGameSessionByID(1, false).Returns(_gameSession);
            _gameSessionRepo.GetCurrentGameSession().Returns(_gameSession);

            var action = new CheckPlayerTurn(_gameSessionRepo, _userSessionRepo);

            //Act
            //...

            //Assert
            Assert.IsTrue(action.Execute(1));
            Assert.IsTrue(action.Execute(_gameSession));
        }

        [Test]
        public void IfOnlyOpponentHasAnsweredThenIsPlayerTurn()
        {
            //Arrange
            _testRound.Player1Answers = new List<Answer>();
            _testRound.Player2Answers = _testAnswerList;
            _gameSession.CurrentRound = _testRound;

            _gameSession.MatchRounds.Add(_testRound);

            _gameSessionRepo.GetGameSessionByID(1, false).Returns(_gameSession);
            _gameSessionRepo.GetCurrentGameSession().Returns(_gameSession);

            var action = new CheckPlayerTurn(_gameSessionRepo, _userSessionRepo);

            //Act
            //...

            //Assert
            Assert.IsTrue(action.Execute(1));
            Assert.IsTrue(action.Execute(_gameSession));
        }
    }
}