﻿using Actions;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace ActionsTests
{
    public class GetPlayerInfoTests
    {
        private IWebPlayerServices _webPlayerRepo;
        private IUserSessionRepo _userSessionRepo;
        private Player _mockPlayer;
        private UserSession _mockUserSession;
        private PlayerData _mockPlayerData;

        [SetUp]
        public void Setup()
        {
            _mockPlayerData = new PlayerData(1, "Elian", new List<PowerUp>());
            _mockPlayer = new Player("eremaggi", _mockPlayerData);
            _mockUserSession = new UserSession(_mockPlayer);
            
            _webPlayerRepo = Substitute.For<IWebPlayerServices>();
            _userSessionRepo = Substitute.For<IUserSessionRepo>();
            _webPlayerRepo.FindPlayerByID("eremaggi").Returns(_mockPlayer);
            _userSessionRepo.GetCurrentUserSession().Returns(_mockUserSession);
        }

        [Test]
        public async void GetPlayerInfoShouldReturnPlayerIDPersonalInfo()
        {
            //Arrange
            //..

            //Act
            GetPlayerInfo action = new GetPlayerInfo(_webPlayerRepo, _userSessionRepo);
            
            //Assert
            Assert.AreEqual(_mockPlayer, await action.Execute());
        }
    }
}