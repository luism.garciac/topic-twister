﻿using Actions;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace ActionsTests
{
    public class CheckMatchFinishedTests
    {
        private IGameSessionRepo _gameSessionRepo;

        private Player _player;
        private Player _opponent;

        [SetUp]
        public void Setup()
        {
            _player = new Player("eremaggi", new PlayerData(3, "Elian", new List<PowerUp>()));
            _opponent = new Player("lgarcia", new PlayerData(1, "Luis", new List<PowerUp>()));

            _gameSessionRepo = Substitute.For<IGameSessionRepo>();
        }

        [Test]
        public void IfWinnerFoundThenMatchFinished()
        {
            //Arrange
            GameSession gameSession = new GameSession(_player, _opponent);
            gameSession.Winner = _player;
            gameSession.IsTie = false;

            var checkMatchFinished = new CheckMatchFinished(_gameSessionRepo);

            _gameSessionRepo.GetCurrentGameSession().Returns(gameSession);
            _gameSessionRepo.GetGameSessionByID(1, false).Returns(gameSession);

            //Act
            //...

            //Assert
            Assert.IsTrue(checkMatchFinished.Execute());
            Assert.IsTrue(checkMatchFinished.Execute(1));
        }

        [Test]
        public void IfMatchTiedThenMatchFinished()
        {
            //Arrange
            GameSession gameSession = new GameSession(_player, _opponent);
            gameSession.IsTie = true;

            var checkMatchFinished = new CheckMatchFinished(_gameSessionRepo);

            _gameSessionRepo.GetCurrentGameSession().Returns(gameSession);
            _gameSessionRepo.GetGameSessionByID(1, false).Returns(gameSession);

            //Act
            //...

            //Assert
            Assert.IsTrue(checkMatchFinished.Execute());
            Assert.IsTrue(checkMatchFinished.Execute(1));
        }
    }
}