﻿using Actions;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace ActionsTests
{
    public class GetCurrentRoundTests
    {
        private IGameSessionRepo _gameSessionRepo;
        private Player _player = new Player("eremaggi", new PlayerData(3, "Elian", new List<PowerUp>()));
        private Player _opponent = new Player("lgarcia", new PlayerData(1, "Luis", new List<PowerUp>()));
        private Round _testRound;

        [SetUp]
        public void Setup()
        {
            _gameSessionRepo = Substitute.For<IGameSessionRepo>();


            GameSession testGameSession = new GameSession(_player, _opponent);
            _testRound = new Round(new List<Category>(), 1);
            testGameSession.CurrentRound = _testRound;
            _gameSessionRepo.GetCurrentGameSession().Returns(testGameSession);
        }

        [Test]
        public void GetCurrentRoundShouldReturnSessionCurrentRound()
        {
            //Arrange
            //Act
            GetCurrentRound getCurrentRound = new GetCurrentRound(_gameSessionRepo);
            //Assert
            Assert.AreEqual(_testRound, getCurrentRound.Execute());
        }
    }
}