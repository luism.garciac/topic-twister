﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepositoryTests
{
    public class InMemoryPlayerRepositoryTests
    {
        [Test]
        public static void PlayerRepositoryShouldFindPlayerByID()
        {
            //Arrange
            InMemoryPlayerRepo playerRepository = new InMemoryPlayerRepo();
            //Act
            Player player = playerRepository.FindPlayerById("1");
            //Assert
            Assert.IsNotNull(player);
        }

        [Test]
        public static void PlayerRepositoryShouldReturnExceptionIfIDNotFound()
        {
            //Arrange
            InMemoryPlayerRepo playerRepository = new InMemoryPlayerRepo();
            //Act
            //Assert
            var exception = Assert.Throws<Exception>(() => playerRepository.FindPlayerById("999"));
            Assert.AreEqual(exception.Message, "Player Not Found");

        }
    }
}