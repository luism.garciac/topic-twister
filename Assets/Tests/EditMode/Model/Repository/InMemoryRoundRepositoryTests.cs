﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;

namespace InMemoryRepositoryTests
{
    public class InMemoryRoundRepositoryTests
    {
        private IRoundRepo _roundRepository;
        private List<Category> _testCategories = new List<Category>();
        private Round _roundOne;
        private Round _roundTwo;
        private Round _roundThree;

        [SetUp]
        public void Setup()
        {
            _roundRepository = Substitute.For<IRoundRepo>();
            _roundRepository.ClearAllRounds();

            Category testCategory = new Category(1, "TestCategory", new List<string>() { "Answer" });
            
            _testCategories.Add(testCategory);
            _testCategories.Add(testCategory);
            _testCategories.Add(testCategory);
            _testCategories.Add(testCategory);
            _testCategories.Add(testCategory);


            _roundOne = new Round(_testCategories, 1);
            _roundTwo = new Round(_testCategories, 2);
            _roundThree = new Round(_testCategories, 3);
        }


        [Test]
        public void Get_Session_Rounds_Should_Return_The_Current_List_Of_Rounds()
        {
            //Arrange
            List<Round> expectedList = new List<Round>();
            expectedList.Add(_roundOne);
            expectedList.Add(_roundTwo);
            expectedList.Add(_roundThree);

            //Act
            _roundRepository.CreateNewRound(_testCategories).Returns(_roundOne);
            _roundRepository.CreateNewRound(_testCategories);
            _roundRepository.CreateNewRound(_testCategories).Returns(_roundTwo);
            _roundRepository.CreateNewRound(_testCategories);
            _roundRepository.CreateNewRound(_testCategories).Returns(_roundThree);
            _roundRepository.CreateNewRound(_testCategories);

            _roundRepository.GetSessionRounds().Returns(expectedList);           

            //Assert
            Assert.AreEqual(expectedList, _roundRepository.GetSessionRounds());
        }

        [Test] 
        public void Create_New_Round_Should_Return_A_New_Round()
        {
            //Arrange
            Round createdRound;
            IRoundRepo roundRepository = RepoLocator.GetRoundRepo(); 

            //Act
            createdRound = roundRepository.CreateNewRound(_testCategories);

            //Assert
            Assert.IsInstanceOf(typeof(Round), createdRound);
        }

        [Test]
        public void Save_Round_Answers_Should_Save_Answers_In_Current_Round()
        {
            //Arrange
            List<string> answers = new List<string> {"uno","dos","tres","cuatro","cinco"};
            List<string> playerAnswers = new List<string>();
            IRoundRepo roundRepository = RepoLocator.GetRoundRepo();

            //Act
            roundRepository.CreateNewRound(_testCategories);
            roundRepository.SaveRoundAnswers(answers);
            foreach(var answer in roundRepository.GetCurrentRound().Player1Answers)
            {
                playerAnswers.Add(answer.PlayerAnswer);
            }

            //Assert
            Assert.AreEqual(answers,playerAnswers);
        }

        [Test]
        public void Update_Round_Letter_Should_Set_The_Current_Round_Letter()
        {
            //Arrange
            char expectedLetter = 'A';
            char recievedLetter;
            IRoundRepo roundRepository = RepoLocator.GetRoundRepo();

            //Act
            roundRepository.CreateNewRound(_testCategories);
            roundRepository.UpdateRoundLetter(expectedLetter);
            recievedLetter = roundRepository.GetCurrentRound().RoundLetter;
            //Assert
            Assert.AreEqual(expectedLetter, recievedLetter);
        }
    }
}