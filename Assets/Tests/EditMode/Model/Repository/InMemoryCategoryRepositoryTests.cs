﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepositoryTests
{
    public class InMemoryCategoryRepositoryTests
    {
        //
        [Test]
        public static void RepositoryShouldReturnFiveElements()
        {
            //Arrange
            InMemoryCategoryRepo categoryRepository = new InMemoryCategoryRepo();
            //Act
            List<Category> categoryList = categoryRepository.FindRandomCategoryList();
            //Assert
            Assert.AreEqual(5, categoryList.Count);
        }

        [Test]
        public static void RepositoryShouldReturnFiveDifferentElements()
        {
            //Arrange
            InMemoryCategoryRepo categoryRepository = new InMemoryCategoryRepo();
            //Act
            List<Category> categoryList = categoryRepository.FindRandomCategoryList();
            //Assert
            foreach (Category category in categoryList)
            {
                Assert.AreEqual(1, categoryList.FindAll(s => s.Equals(category)).Count);
            }
        }
    }
}