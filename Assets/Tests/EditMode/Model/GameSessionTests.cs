using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class GameSessionTests
{
    #region GameSessionTests

    ////Una Sesion de juego puede tener como maximo 3 rondas
    //[Test]
    //public static void A_Game_Session_Should_Have_No_More_Than_3_Rounds()
    //{
    //    //Arrange
    //    GameSession gameSession = new GameSession(new Player(1, new PlayerData()), new Player(2, new PlayerData()));
    //    //Act
    //    gameSession.StartSession();
    //    gameSession.CreateNewRound();
    //    gameSession.CreateNewRound();
    //    gameSession.CreateNewRound();
    //    gameSession.CreateNewRound();
    //    gameSession.CreateNewRound();
    //    //Assert
    //    Assert.AreEqual(3, gameSession.MyRounds.Count());
    //}

    //[Test]
    //public static void A_Game_Session_Should_Start_With_The_First_Round()
    //{
    //    //Arrange
    //    GameSession gameSession = new GameSession(new Player(1, new PlayerData()), new Player(2, new PlayerData()));
    //    //Act
    //    gameSession.StartSession();
    //    //Assert
    //    Assert.AreEqual(1, gameSession.CurrentRoundNumber);
    //}


    //// Si el player tiene mas victorias que el oponente, gana la partida
    //[Test]
    //public static void If_The_Player_Has_More_Victories_Than_His_Opponent_He_Wins_The_Match()
    //{
    //    //Arrange
    //    GameSession gameSession = new GameSession(new Player(1, new PlayerData()), new Player(2, new PlayerData()));
    //    //Act
    //    Round round = Substitute.For<Round>(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player1;
    //    round.Score = 10;
    //    gameSession.MyRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player1;
    //    round.Score = 10;
    //    gameSession.MyRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player1;
    //    round.Score = 10;
    //    gameSession.MyRounds.Add(round);

    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player2;
    //    round.Score = 9;
    //    gameSession.OpponentRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player2;
    //    round.Score = 9;
    //    gameSession.OpponentRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player2;
    //    round.Score = 9;
    //    gameSession.OpponentRounds.Add(round);

    //    gameSession.FinishSession();

    //    //Assert
    //    Assert.AreEqual(gameSession.Player1,gameSession.Winner);
    //}

    //// Si el oponente tiene mas victorias que el player, gana la partida
    //[Test]
    //public static void If_The_Opponent_Has_More_Victories_Than_The_Player_He_Wins_The_Match()
    //{
    //    //Arrange
    //    GameSession gameSession = new GameSession(new Player(1, new PlayerData()), new Player(2, new PlayerData()));
    //    //Act
    //    Round round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player1;
    //    round.Score = 5;
    //    gameSession.MyRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player1;
    //    round.Score = 6;
    //    gameSession.MyRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player1;
    //    round.Score = 3;
    //    gameSession.MyRounds.Add(round);

    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player2;
    //    round.Score = 8;
    //    gameSession.OpponentRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player2;
    //    round.Score = 10;
    //    gameSession.OpponentRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player2;
    //    round.Score = 7;
    //    gameSession.OpponentRounds.Add(round);

    //    gameSession.FinishSession();

    //    //Assert
    //    Assert.AreEqual(gameSession.Player2, gameSession.Winner);
    //}

    //[Test]

    //public static void If_All_The_Rounds_Are_Tie_The_Match_Is_Tie()
    //{
    //    //Arrange
    //    GameSession gameSession = new GameSession(new Player(1, new PlayerData()), new Player(2, new PlayerData()));
    //    //Act
    //    Round round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player1;
    //    round.Score = 10;
    //    gameSession.MyRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player1;
    //    round.Score = 10;
    //    gameSession.MyRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player1;
    //    round.Score = 10;
    //    gameSession.MyRounds.Add(round);

    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player2;
    //    round.Score = 10;
    //    gameSession.OpponentRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player2;
    //    round.Score = 10;
    //    gameSession.OpponentRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player2;
    //    round.Score = 10;
    //    gameSession.OpponentRounds.Add(round);

    //    gameSession.FinishSession();

    //    //Assert
    //    Assert.IsTrue(gameSession.IsTie);
    //}

    //[Test]

    //public static void If_The_Amount_Of_Wins_For_Each_Player_Is_Equal_The_Match_Is_A_Tie()
    //{
    //    //Arrange
    //    GameSession gameSession = new GameSession(new Player(1, new PlayerData()), new Player(2, new PlayerData()));
    //    //Act
    //    Round round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player1;
    //    round.Score = 10;
    //    gameSession.MyRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player1;
    //    round.Score = 10;
    //    gameSession.MyRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player1;
    //    round.Score = 9;
    //    gameSession.MyRounds.Add(round);

    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player2;
    //    round.Score = 10;
    //    gameSession.OpponentRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player2;
    //    round.Score = 9;
    //    gameSession.OpponentRounds.Add(round);
    //    round = new Round(RepositoryServiceLocator.GetCategoryRepository());
    //    round.Player = gameSession.Player2;
    //    round.Score = 10;
    //    gameSession.OpponentRounds.Add(round);

    //    gameSession.FinishSession();

    //    //Assert
    //    Assert.IsTrue(gameSession.IsTie);
    //}

    ////Una Sesion de juego no puede finalizar sin tener al menos de 3 Rondas
    //[Test]
    //public static void A_Game_Session_Will_Not_Finish_Unless_It_Has_3_Rounds()
    //{
    //    //Arrange
    //    GameSession gameSession = new GameSession(new Player(1, new PlayerData()), new Player(2, new PlayerData()));
    //    //Act
    //    gameSession.StartSession();
    //    gameSession.CreateNewRound();
    //    gameSession.FinishSession();
    //    //Assert
    //    Assert.Throws<Exception>(() => gameSession.FinishSession());
    //}

    #endregion
}
