﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ServiceProviderTests
{
    Player _testPlayer;
    [SetUp]
    public void Setup()
    {
        _testPlayer = new Player("1", new PlayerData(3, "Luis", new List<PowerUp>()));
    }

    [Test]
    public void ServiceProviderShouldBeAbleToCreateANewGame()
    {
        //Arrange
        //Act
        ServiceProvider.CreateTopicTwisterGame(_testPlayer);
        //Assert
        Assert.IsNotNull(ServiceProvider.GetTopicTwisterGame());
    }
}

