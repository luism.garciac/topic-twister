﻿using NUnit.Framework;
using NSubstitute;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;

namespace WebServicesTests
{
    public class WebLoginServicesTests
    {
        IWebLoginService _webloginService;
        Player _mockPlayer;
        Player _realPlayer;

        [SetUp]
        public void Setup()
        {
            _realPlayer = new Player("lgarcia", new PlayerData(1, "Luis", new List<PowerUp>()));
            _mockPlayer = new Player("pepe", new PlayerData(1, "Pepe", new List<PowerUp>()));
        }

        [Test]
        public async void LoginServicesShouldValidatePlayerLoginMocked()
        {
            //Arrange
            Player testPlayer;
            _webloginService = Substitute.For<IWebLoginService>();
            _webloginService.FindPlayerByID("pepe").Returns(_mockPlayer);
            //Act
            testPlayer = await _webloginService.FindPlayerByID("pepe");
            //Assert
            Assert.AreEqual(_mockPlayer, testPlayer);
        }

        [Test]
        public async void LoginServicesShouldValidatePlayerLoginReal()
        {
            //Arrange
            Player testPlayer;
            WebLoginServices webLoginService = new WebLoginServices("https://login-topic-twister.herokuapp.com/");

            //Act
            //testPlayer = await webLoginService.FindPlayerByID(_realPlayer.UserID);

            //Assert
            Assert.AreEqual(_realPlayer.UserID, await Task.Run(async () =>
            {
                testPlayer = await webLoginService.FindPlayerByID(_realPlayer.UserID);
                return testPlayer.UserID;
            }));
        }
    }
}
