﻿using Actions;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace PresentationTests
{
    public class RollScreenPresenterTests
    {
        private RollScreenPresenter _rollScreenPresenter;
        private IRollScreenView _rollScreenView;
        private ICategoryRepo _categoryRepository;
        private IWebCategoryServices _webCategoryServices;
        private IRoundRepo _roundRepository;
        private IGameSessionRepo _gameSessionRepository;
        private IUserSessionRepo _userSessionRepo;

        private List<Category> _testCategories = new List<Category>();
        private List<string> _testCategoryNames = new List<string>();
        private GetRoundCategoryNames _getRoundCategoryNames;
        private GetSessionOpponent _getSessionOpponent;

        private char _testLetter = 'A';

        [SetUp]
        public void Setup()
        {
            _rollScreenView = Substitute.For<IRollScreenView>();
            _categoryRepository = Substitute.For<ICategoryRepo>();
            _webCategoryServices = Substitute.For<IWebCategoryServices>();
            _roundRepository = Substitute.For<IRoundRepo>();
            _gameSessionRepository = Substitute.For<IGameSessionRepo>();
            _userSessionRepo = Substitute.For<IUserSessionRepo>();

            _getRoundCategoryNames = Substitute.For<GetRoundCategoryNames>(_gameSessionRepository);
            _getSessionOpponent = Substitute.For<GetSessionOpponent>(_gameSessionRepository, _userSessionRepo);

            //Category Repository Mocking
            Category testCategory = new Category(1, "TestCategory", new List<string>() { "Answer" });

            _testCategories.Add(testCategory);
            _testCategories.Add(testCategory);
            _testCategories.Add(testCategory);
            _testCategories.Add(testCategory);
            _testCategories.Add(testCategory);

            _webCategoryServices.GetRandomCategoryList().Returns(_testCategories);

            //GetRoundCategoryNames Mocking

            foreach (Category category in _testCategories)
            {
                _testCategoryNames.Add(category.CategoryName);
            }

            _getRoundCategoryNames.Execute().Returns(_testCategoryNames);

            //Round Repository Mocking

            Round testRound = new Round(_testCategories, 1);
            testRound.RoundLetter = _testLetter;
            _roundRepository.CreateNewRound(_testCategories).Returns(testRound);
            _roundRepository.GetCurrentRound().Returns(testRound);

            //Game Session Repository Mocking

            Player player1 = new Player("lgarcia", new PlayerData(1, "Luis", new List<PowerUp>()));
            Player player2 = new Player("eremaggi", new PlayerData(2, "Elian", new List<PowerUp>()));
            GameSession testGameSession = new GameSession(player1, player2);

            testGameSession.CurrentRound = testRound;

            _gameSessionRepository.GetCurrentGameSession().Returns(testGameSession);

            //Opponent Mock

            _userSessionRepo.GetCurrentUserSession().Returns(new UserSession(player1));

            //Create Presenter

            _rollScreenPresenter = new RollScreenPresenter(_rollScreenView,
                                                           _getRoundCategoryNames,
                                                           new GetSessionOpponent(_gameSessionRepository, _userSessionRepo),
                                                           new GetCurrentRound(_gameSessionRepository));
        }

        [Test]
        public void A_New_Round_Should_Update_Round_Number()
        {
            //Arrange

            //Act
            _rollScreenView.OnInitRollScreenEvent += Raise.Event<Action>();
            _rollScreenView.OnUpdateRoundNumberEvent += Raise.Event<Action>();

            //Assert
            _rollScreenView.Received().UpdateRoundNumber(1);
        }

        [Test]
        public void A_New_Round_Should_Update_Opponent_Name()
        {
            //Arrange

            //Act
            _rollScreenView.OnUpdateOpponentNameEvent += Raise.Event<Action>();

            //Assert
            _rollScreenView.Received().UpdateOpponentName("Elian");
        }

        [Test]
        public void A_New_Round_Should_Update_Category_Names()
        {
            //Arrange
            //Act
            _rollScreenView.OnUpdateCategoryNames += Raise.Event<Action>();
            //Assert
            _rollScreenView.Received().UpdateCategories(_testCategoryNames);
        }

        [Test]
        public void Pressing_Roll_Updates_Screen_Round_Letter()
        {
            //Arrange
            //Act
            _rollScreenView.OnInitRollScreenEvent += Raise.Event<Action>();
            _rollScreenView.OnRollButtonClickEvent += Raise.Event<Action>();
            //Assert
            _rollScreenView.Received().UpdateRoundLetter(_testLetter);
        }
    }
}