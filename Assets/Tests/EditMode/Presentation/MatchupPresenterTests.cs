﻿using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Reactive;
using Actions;

namespace PresentationTests
{
    public class MatchupPresenterTests
    {
        private MatchupScreenPresenter _matchupScreenPresenter;
        private IMatchupScreenView _matchupScreenView;
        private IWebPlayerServices _webPlayerRepository;
        private IWebMatchMakingServices _webMatchMakingServices;
        private IWebGameSessionServices _webSessionsRepo;
        private IGameSessionRepo _gameSessionRepo;
        private IUserSessionRepo _userSessionRepo;
        private FindMatch _findMatch;
        private GetPlayerInfo _getPlayerInfo;
        private GetPlayerSessions _getPlayerGameSessions;
        private CheckMatchFinished _checkMatchFinished;
        private CheckPlayerTurn _checkPlayerTurn;
        private GetLoggedPlayer _getLoggedPlayer;
        private SessionButtonClickEvent _sessionButtonClickEvent;

        private Player _player1;
        private Player _opponent;
        private GameSession _testGameSession;
        private UserSession _mockUserSession;
        private List<GameSession> _mockGameSessions = new List<GameSession>();

        [SetUp]
        public void Setup()
        {
            //Repositories Substitutes
            _webPlayerRepository = Substitute.For<IWebPlayerServices>();
            _webMatchMakingServices = Substitute.For<IWebMatchMakingServices>();
            _webSessionsRepo = Substitute.For<IWebGameSessionServices>();
            _matchupScreenView = Substitute.For<IMatchupScreenView>();
            _gameSessionRepo = Substitute.For<IGameSessionRepo>();
            _userSessionRepo = Substitute.For<IUserSessionRepo>();

            //Use Cases Substitues
            _getPlayerInfo = Substitute.For<GetPlayerInfo>(_webPlayerRepository, _userSessionRepo);
            _findMatch = Substitute.For<FindMatch>(_webMatchMakingServices);
            _getPlayerGameSessions = Substitute.For<GetPlayerSessions>(_webSessionsRepo, _userSessionRepo);
            _checkPlayerTurn = Substitute.For<CheckPlayerTurn>(_gameSessionRepo, _userSessionRepo);
            _checkMatchFinished = Substitute.For<CheckMatchFinished>(_gameSessionRepo);
            _getLoggedPlayer = Substitute.For<GetLoggedPlayer>(_userSessionRepo);

            //Mock Player Repository
            _player1 = new Player("lgarcia", new PlayerData(4, "Luis", new List<PowerUp>()));
            _opponent = new Player("eremaggi", new PlayerData(2, "Elian", new List<PowerUp>()));

            _webPlayerRepository.FindPlayerByID("lgarcia").Returns(_player1);

            //Mock User Session
            _mockUserSession = new UserSession(_player1);
            _userSessionRepo.GetCurrentUserSession().Returns(_mockUserSession);

            //Mock Web MatchMaking Services
            _testGameSession = new GameSession(_player1, _opponent);
            _testGameSession.SessionID = 1;
            _testGameSession.Player1Score = 1;
            _testGameSession.Player2Score = 0;
            _webMatchMakingServices.FindOpponent(_player1.PlayerData.Name).Returns(_testGameSession);

            //Mock Game Session Repo
            _gameSessionRepo.GetGameSessionByID(1,false).Returns(_testGameSession);

            //Mock Player Info
            _getPlayerInfo.Execute().Returns(_player1);

            //Mock Find Match
            _findMatch.Execute(_player1).Returns(_testGameSession);

            //Mock Get Player Game Sessions
            _mockGameSessions.Add(_testGameSession);
            _mockGameSessions.Add(_testGameSession);
            _mockGameSessions.Add(_testGameSession);

            _getPlayerGameSessions.Execute().Returns(_mockGameSessions);

            //Mock Check Player Turn
            _checkPlayerTurn.Execute(1).Returns(true);
            _checkPlayerTurn.Execute(_testGameSession).Returns(true);

            //Mock Check Match Finished
            _checkMatchFinished.Execute(1).Returns(false);
            _checkMatchFinished.Execute().Returns(false);

            //Mock Get Logged Player
            _getLoggedPlayer.Execute().Returns(_player1);

            //Build Presenter
            _matchupScreenPresenter = new MatchupScreenPresenter(_matchupScreenView, _getPlayerInfo, _findMatch, _getPlayerGameSessions, 
                                                                 _checkMatchFinished, _checkPlayerTurn, _getLoggedPlayer, _sessionButtonClickEvent);
        }

        [Test]
        public void ShowUserNameOnScreenStart() 
        {
            //Arrange
            //..

            //Act
            _matchupScreenView.OnPlayerNameUpdateEvent += Raise.Event<Action>();

            //Assert
            _matchupScreenView.Received().UpdateUserName("Luis");
        }

        [Test]
        public void ShowWinsAmountOnScreenStart() 
        {
            //Arrange
            //..

            //Act
            _matchupScreenView.OnPlayerWinsUpdateEvent += Raise.Event<Action>();

            //Assert
            _matchupScreenView.Received().UpdateWinsAmount(4);
        }

        [Test]
        public void ResetPlayerSessionListOnScreenStart()
        {
            //Arrange
            //..

            //Act
            _matchupScreenView.OnMatchupScreenInitEvent += Raise.Event<Action>();

            //Assert
            _matchupScreenView.Received().ResetPanels();
        }

        [Test]
        public void InitPlayerSessionListOnScreenStart() 
        {
            //Arrange
            //..

            //Act
            _matchupScreenView.OnMatchupScreenInitEvent += Raise.Event<Action>();

            //Assert
            _matchupScreenView.Received(3).InitSessionContainer(_testGameSession.SessionID, _testGameSession.Player1Score, _testGameSession.Player2Score, 
                                                                _testGameSession.Player1.PlayerData.Name, _testGameSession.Player2.PlayerData.Name,
                                                                false,true);
        }

        [Test]
        public void WhenPlayerPressFindMatchCallNextScreen()
        {
            //Arrange
            //..

            //Act
            _matchupScreenView.OnFindMatchButtonClickEvent += Raise.Event<Action>();

            //Assert
            _matchupScreenView.Received().NextScreen();
        }
    }
}