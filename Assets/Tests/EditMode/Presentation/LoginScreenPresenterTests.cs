﻿using Actions;
using NSubstitute;
using NUnit.Framework;
using Presentation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PresentationTests
{
    public class LoginScreenPresenterTests
    {
        private LoginScreenPresenter _loginScreenPresenter;
        private ILoginScreenView _loginScreenView;
        private IWebLoginService _webLoginService;
        private IUserSessionRepo _userSessionRepository;

        [SetUp]
        public void Setup()
        {
            _loginScreenView = Substitute.For<ILoginScreenView>();
            _webLoginService = Substitute.For<IWebLoginService>();
            _userSessionRepository = Substitute.For<IUserSessionRepo>();
            _loginScreenPresenter = new LoginScreenPresenter(_loginScreenView, new Login(_userSessionRepository, _webLoginService));
        }

        [Test]
        public void ShowPromptIfNoUserIsEntered()
        {
            //Arrange
            string userID = "";
            //Act
            _loginScreenView.OnLoginButtonClickEvent += Raise.Event<Action<string>>(userID);
            //Assert
            _loginScreenView.Received().ShowPopUp("Ingrese Nombre de Usuario");
        }

        [Test]
        public void ShowPromptIfNoUserNotFound()
        {
            //Arrange
            string userID = "1";
            _webLoginService.FindPlayerByID(userID).Returns(Task.FromException<Player>(new Exception()));
            //Act
            _loginScreenView.OnLoginButtonClickEvent += Raise.Event<Action<string>>(userID);
            //Assert
            _loginScreenView.Received().ShowPopUp("Usuario no encontrado");
        }

        [Test]
        public void NavigateToNextScreenWhenLoginSuccessfull()
        {
            //Arrange
            string userID = "1";
            _webLoginService.FindPlayerByID(userID).Returns(new Player("1", new PlayerData(1, "Sebastian", new List<PowerUp>())));
            //Act
            _loginScreenView.OnLoginButtonClickEvent += Raise.Event<Action<string>>(userID);
            //Assert
            _loginScreenView.Received().NextScreen();
        }
    }
}