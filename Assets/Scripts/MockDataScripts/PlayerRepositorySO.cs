using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerRepository", menuName = "PlayerRepository")]

public class PlayerRepositorySO : ScriptableObject
{
    public List<PlayerSerializable> _playerList;
}


