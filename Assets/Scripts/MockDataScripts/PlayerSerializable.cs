using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerSerializable
{
    public string _userID;
    public PlayerDataSerializable _playerData;
}
