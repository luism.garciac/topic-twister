using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerDataSerializable
{
    public int _winsAmount;
    public string _name;
}
