﻿using UniRx;
using UnityEngine;

namespace Reactive
{
    [CreateAssetMenu(fileName = "SessionButtonClickEvent", menuName = "SessionButtonClickEvent")]

    public class SessionButtonClickEvent : ScriptableObject
    {
        public ReactiveProperty<bool> sessionButtonClicked = new ReactiveProperty<bool>(false);
    }
}
