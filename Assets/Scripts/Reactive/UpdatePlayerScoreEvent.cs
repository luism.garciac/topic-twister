﻿using UniRx;
using UnityEngine;

namespace Reactive
{
    [CreateAssetMenu(fileName = "UpdatePlayerScoreEvent", menuName = "UpdatePlayerScoreEvent")]

    public class UpdatePlayerScoreEvent : ScriptableObject
    {
        public ReactiveProperty<int> playerScoreChanged = new ReactiveProperty<int>(0);
    }
}
