using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]

public class AnimationVFX : MonoBehaviour
{
    private Animator animator;
    private AudioSource audioSource;
    private RuntimeAnimatorController controller;
    private AudioClip clip;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();

        controller = (RuntimeAnimatorController)Resources.Load("Animators/AnimationVFX");
        clip = (AudioClip)Resources.Load("Sounds/popUp");
        
        animator.runtimeAnimatorController = controller;
        audioSource.clip = clip;
        audioSource.Play();
    }
    private void OnEnable()
    {
        audioSource.Play();
    }
}
