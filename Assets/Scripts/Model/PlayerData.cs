﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerData
{
    private int _winsAmount;
    private string _name;
    private List<PowerUp> _powerUps;

    public PlayerData(int winsAmount, string name, List<PowerUp> powerUps)
    {
        _winsAmount = winsAmount;
        _name = name;
        _powerUps = powerUps;
    }

    public int WinsAmount { get => _winsAmount; set => _winsAmount = value; }
    public string Name { get => _name;}
}