﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class InMemoryGameSessionRepo : IGameSessionRepo
{
    private GameSession _currentGameSession;
    private List<GameSession> _gameSessions = new List<GameSession>();

    public GameSession CreateNewGameSession(Player player1, Player player2)
    {
        _currentGameSession = new GameSession(player1, player2);
        _gameSessions.Add(_currentGameSession);
        return _currentGameSession;
    }

    public GameSession GetCurrentGameSession()
    {
        return _currentGameSession;
    }

    public void SaveGameSession(GameSession gameSession)
    {
        int sessionIndex = 0;
        foreach (var session in _gameSessions)
        {
            sessionIndex++;
            if (session.SessionID == gameSession.SessionID)
            {
                _gameSessions[sessionIndex] = gameSession;
                break;
            }
        }
    }

    public GameSession GetGameSessionByID(int sessionID, bool updateCurrent = true)
    {
        GameSession currentGameSession = _gameSessions.FirstOrDefault(q => q.SessionID == sessionID);

        if (updateCurrent)
            _currentGameSession = currentGameSession;

        return currentGameSession;
    }

    public void SetCurrentGameSession(GameSession gameSession)
    {
        _currentGameSession = gameSession;
    }

    public void SetGameSessions(List<GameSession> gameSessions)
    {
        _gameSessions = gameSessions;
    }

    public void SaveRoundAnswers(List<string> answers, string userID)
    {
        List<Answer> playerAnswers = new List<Answer>();
        int lastRoundIndex = _currentGameSession.MatchRounds.Count - 1;

        foreach (var answer in answers)
        {
            Answer playerAnswer = new Answer(answer, false);
            playerAnswers.Add(playerAnswer);
        }

        if (_currentGameSession.Player1.UserID == userID)
        {
            _currentGameSession.MatchRounds[lastRoundIndex].Player1Answers = playerAnswers;
            _currentGameSession.CurrentRound.Player1Answers = playerAnswers;
        }
        else if (_currentGameSession.Player2.UserID == userID)
        {
            _currentGameSession.MatchRounds[lastRoundIndex].Player2Answers = playerAnswers;
            _currentGameSession.CurrentRound.Player2Answers = playerAnswers;
        }
    }


}

