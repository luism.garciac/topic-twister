﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class InMemoryRoundRepo: IRoundRepo
{
    private int _sessionID;
    private Round _currentRound;
    private List<Round> _matchRounds = new List<Round>();

    public void GetSessionRoundData(int sessionID)
    {

    }

    public List<Round> GetSessionRounds()
    {
        return _matchRounds;
    }

    public Round GetCurrentRound()
    {
        return _currentRound;
    }

    public Round CreateNewRound(List<Category> categories)
    {
        int roundNumber = _matchRounds.Count + 1;
        _currentRound = new Round(categories, roundNumber);
        _matchRounds.Add(_currentRound);
        return _currentRound;
    }

    public void SaveRoundAnswers(List<string> answers)
    {
        List<Answer> playerAnswers = new List<Answer>();
        foreach (var answer in answers)
        {
            Answer playerAnswer = new Answer(answer, false);
            _currentRound.Player1Answers.Add(playerAnswer);
        }
    }

    public void UpdateRoundLetter(char roundLetter)
    {
        _currentRound.RoundLetter = roundLetter;
    }

    //public void CreateOpponentAnswersSimulated()
    //{
    //    ValidateAnswer validateAnswer;

    //    for (int i = 0; i < 5; i++)
    //    {
    //        string answer = String.Empty;
    //        Category category = _currentRound.Categories[i];

    //        if (UnityEngine.Random.Range(0, 10) > 4) //Simulate Correct Answer
    //        {
    //            char roundLetter = _currentRound.RoundLetter;
    //            answer = category.WordList.First(s => s[0] == roundLetter);
    //            Answer playerAnswer = new Answer(answer, true);
    //            _currentRound.Player2Answers.Add(playerAnswer);
    //        }
    //        else //Simulate Wrong Answer
    //        {
    //            answer = "N/A";
    //            Answer playerAnswer = new Answer(answer, false);
    //            _currentRound.Player2Answers.Add(playerAnswer);
    //        }

    //        validateAnswer = new ValidateAnswer(answer, category);
            
    //        if (validateAnswer.Execute())
    //        {
    //            _currentRound.Player2Score++;
    //        }
    //    }
    //}

    public void ClearAllRounds()
    {
        _matchRounds.Clear();
        _currentRound = null;
    }
}

