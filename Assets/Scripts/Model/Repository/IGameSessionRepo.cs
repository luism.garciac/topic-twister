﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public interface IGameSessionRepo
{
    GameSession GetCurrentGameSession();
    void SetCurrentGameSession(GameSession gameSession);
    GameSession CreateNewGameSession(Player player1, Player player2);
    void SaveRoundAnswers(List<string> answers, string userID);
    GameSession GetGameSessionByID(int sessionID, bool updateCurrent = true);
    void SetGameSessions(List<GameSession> gameSessions);

}


