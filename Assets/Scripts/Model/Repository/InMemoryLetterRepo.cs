﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class InMemoryLetterRepo : ILetterRepo
{
    private List<char> _letterList;

    public InMemoryLetterRepo()
    {
        _letterList = new List<char> { 'A', 'E', 'J', 'O', 'M'};
    }

    public char GetRandomLetter()
    {
        return _letterList[UnityEngine.Random.Range(0, _letterList.Count)];
    }
}
