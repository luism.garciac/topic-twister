﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class InMemoryUserSessionRepo : IUserSessionRepo
{
    private UserSession _topicTwisterGame;

    public UserSession CreateUserSession(Player myPlayer)
    {
        _topicTwisterGame = new UserSession(myPlayer);
        return _topicTwisterGame;
    }

    public UserSession GetCurrentUserSession()
    {
        return _topicTwisterGame;
    }
}

