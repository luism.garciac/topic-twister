﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public interface IRoundRepo
{
    Round GetCurrentRound();
    //Round GetOpponentCurrentRound();

    Round CreateNewRound(List<Category> categories);

    void UpdateRoundLetter(char roundLetter);
    //void CreateOpponentAnswersSimulated();
    List<Round> GetSessionRounds();
    //List<Round> GetOpponentRounds();
    void ClearAllRounds();

    void SaveRoundAnswers(List<string> answers);

}

