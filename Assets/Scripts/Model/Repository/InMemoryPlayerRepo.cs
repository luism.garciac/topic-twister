﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class InMemoryPlayerRepo : IPlayerRepo
{
    private PlayerRepositorySO _playerRepositorySO;
    private List<Player> _playerList = new List<Player>();

    public InMemoryPlayerRepo()
    {
        _playerRepositorySO = (PlayerRepositorySO)Resources.Load("PlayerRepository");
        CreatePlayerList();
    }

    private void CreatePlayerList()
    {
        foreach (var _playerSO in _playerRepositorySO._playerList)
        {
            _playerList.Add(new Player(_playerSO._userID,
                            new PlayerData(_playerSO._playerData._winsAmount,
                                           _playerSO._playerData._name,
                                           new List<PowerUp>())));
        }
    }

    public InMemoryPlayerRepo(List<Player> playerList)
    {
        _playerList = playerList;
    }

    public Player FindPlayerById(string ID)
    {
        Player playerFound;
        playerFound = _playerList.FirstOrDefault(q => q.UserID == ID);

        if (playerFound != null)
            return playerFound;
        else
            throw new Exception("Player Not Found");
    }

    public List<Player> FindPlayersLookingForMatch()
    {
        return _playerList;
    }

    public void UpdatePlayerData(string playerId, PlayerData playerData)
    {
        FindPlayerById(playerId).PlayerData = playerData;
    }

    public void ClearPlayerRepository()
    {
        _playerList = new List<Player>();
    }

    public void UpdateRepositoryData(List<Player> playerList)
    {
        _playerList = playerList;
    }
}

