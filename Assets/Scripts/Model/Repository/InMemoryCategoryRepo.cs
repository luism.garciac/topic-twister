﻿using System.Collections.Generic;
using UnityEngine;

public class InMemoryCategoryRepo : ICategoryRepo
{
    private List<Category> _categoryList;

    public InMemoryCategoryRepo()
    {
        _categoryList = new List<Category>() { new Category(1,"Países",  new List<string>            {"Alemania", "Argentina", "Angola", "España", "Etiopia", "Eslovaquia", "Japon", "Jamaica", "Jordania", "Oman", "Mexico", "Malasia", "Madagascar" }),
                                               new Category(2,"Nombres", new List<string>            {"Alberto", "Alejandra", "Ana", "Elian", "Esteban", "Emilia", "Jesus", "Juana", "Jessica", "Omar", "Olivia", "Orlando", "Maria", "Manuel", "Marcos"}),
                                               new Category(3,"Comidas", new List<string>            {"Arroz", "Atun", "Almejas", "Ensalada", "Empanada", "Escabeche", "Jalapeño", "Jamon", "Jardinera", "Orejones", "Olivas", "Omelet", "Milanesa", "Mermelada", "Macarrones" } ),
                                               new Category(4,"Animales", new List<string>           {"Araña", "Avestruz", "Aguila", "Escarabajo", "Elefante", "Escorpion", "Jabali", "Jaguar", "Jirafa", "Oruga", "Oso", "Orangutan", "Mamut", "Mono", "Marmota" }),
                                               new Category(5,"Profesiones", new List<string>        {"Arquitecto", "Albañil", "Agricultor", "Escribano", "Electricista", "Escritor", "Juez", "Jardinero", "Joyero", "Obrero", "Orfebre", "Odontologo", "Marinero", "Maquinista", "Mecanico" }),
                                               new Category(6,"Dioses", new List<string>             {"Afrodita", "Ares","Anubis", "Eco", "Eris", "Eros", "Jupiter", "Jepeto", "Juno", "Osiris", "Orfeo", "Olimpo", "Marte", "Mercurio", "Minerva" }),
                                               new Category(7,"Cosas", new List<string>              {"Almanaque", "Arma", "Auto", "Escalera", "Escoba", "Espada", "Joya", "Jarra", "Jeringa", "Olla", "Oro", "Oreja", "Madera", "Mascara", "Muñeco" }),
                                               new Category(8,"Deportes", new List<string>           {"Alpinismo", "Atletismo", "Automovilismo", "Esgrima", "Esqui", "Escalada", "Judo", "Jujitsu", "Jabalina", "Olimpico", "Motociclismo", "Marcha" }),
                                               new Category(9,"Frutas y Verduras", new List<string>  {"Anana", "Arandano", "Acelga", "Espinaca", "Elote", "Esparragos", "Jengibre", "Judias", "Jitomate", "Oliva", "Oregano", "Ocra", "Manzana", "Mandarina", "Mora" }),
                                               new Category(10,"Marcas", new List<string>            {"Adidas", "Armani", "Axe", "Epson", "Easy", "Elephant", "Jaguar", "Jeep", "Jordan", "Oxea", "Odol", "Oreo", "Mitsubishi", "Michelin", "Minolta" })
                                             };
    }

    public Category FindCategoryById(string ID)
    {
        throw new System.NotImplementedException();
    }

    public List<Category> FindRandomCategoryList()
    {
        List<int> randomNumbers = new List<int>();
        List<Category> randomCategories = new List<Category>();    

        while (randomNumbers.Count < 5)
        {
            int number = Random.Range(0, _categoryList.Count);
            if (randomNumbers.Contains(number))
                continue;
            else
                randomNumbers.Add(number);
        }

        foreach (int index in randomNumbers)
        {
            randomCategories.Add(_categoryList[index]);
        }

        return randomCategories;
    }
}
