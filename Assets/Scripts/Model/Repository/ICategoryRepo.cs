using System.Collections.Generic;

public interface ICategoryRepo 
{
    public Category FindCategoryById(string ID);
    public List<Category> FindRandomCategoryList();
}
