using System;
using TMPro;
using UnityEngine;

public class PopUp : MonoBehaviour
{
	[SerializeField]
	protected TextMeshProUGUI Text;

	public event Action<bool> OnCloseButton;

	public void SetText(string text)
	{
		Text.text = text;
	}

	public void CloseButton()
	{
		OnCloseButton.Invoke(true);
		Destroy(this.gameObject);
	}
}
