﻿using Presentation;
using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ResultScreenView : MonoBehaviour, IResultScreenView
{
    [SerializeField] private Button _endMatchButton;
    [SerializeField] private TMP_Text _winnerText;
    [SerializeField] private GameObject _nextScreen;

    public event Action OnEndMatchButtonClickEvent;
    public event Action OnResultScreenInitEvent;

    private void OnEnable()
    {
        ScreenPresenterBuilder.BuildResultScreenPresenter(this);
        OnResultScreenInitEvent.Invoke();
        _endMatchButton.onClick.AddListener(OnEndMatchButtonClick);
        _endMatchButton.interactable = true;
    }

    private void OnDisable()
    {
        _endMatchButton.onClick.RemoveListener(OnEndMatchButtonClick);
    }

    private void OnEndMatchButtonClick()
    {
        OnEndMatchButtonClickEvent.Invoke();
    }

    public void NextScreen()
    {
        StartCoroutine(SwitchScreen());
    }
    public IEnumerator SwitchScreen()
    {
        _endMatchButton.interactable = false;
        yield return new WaitForSeconds(2f);

        _nextScreen.SetActive(true);
        this.gameObject.SetActive(false);
    }

    public void UpdateWinner(string winner)
    {
        _winnerText.text = winner;
    }
}
