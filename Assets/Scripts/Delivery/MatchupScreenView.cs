﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Reactive;
using UniRx;
using Actions;
using Presentation;

namespace Delivery
{
    public class MatchupScreenView : MonoBehaviour, IMatchupScreenView
    {
        [SerializeField] private Button _findMatchButton;
        [SerializeField] private Button _refreshMatchesButton;
        [SerializeField] private GameObject _nextScreen;
        [SerializeField] private TMP_Text _userTitleName;
        [SerializeField] private TMP_Text _playerWinsText;
        [SerializeField] private GameObject _sessionPrefab;
        [SerializeField] private Transform _sessionListContent;
        [SerializeField] private GameObject _roundResultsScreen;
        [SerializeField] private GameObject _resultScreen;

        [SerializeField] private UpdatePlayerScoreEvent _updateSessionEvent;
        [SerializeField] private SessionButtonClickEvent _sessionButtonClickEvent;

        [SerializeField] private CanvasGroup _matchesListCanvasGroup;

        private MatchupScreenPresenter _matchupScreenViewPresenter;

        public event Action OnFindMatchButtonClickEvent;
        public event Action OnMatchupScreenInitEvent;
        public event Action OnMatchupScreenUpdateEvent;
        public event Action OnResumeMatchButtonClickEvent;
        public event Action<int> OnShowMatchResults;
        public event Action<int> OnResumeMatch;
        public event Action OnSwitchScreenEvent;
        public event Action OnPlayerNameUpdateEvent;
        public event Action OnPlayerWinsUpdateEvent;
        public event Action OnRefreshMatchesButtonClickEvent;

        private Dictionary<int, GameObject> _sessionPanels = new Dictionary<int, GameObject>();

        private bool isPolling = true;

        private void Start()
        {
            InitMatchScreenParameters();
        }

        private void OnEnable()
        {

            _matchupScreenViewPresenter = ScreenPresenterBuilder.BuildMatchupScreenPresenter(this, new GetPlayerInfo(WebServices.GetWebPlayerServices(), RepoLocator.GetUserSessionRepo()),
                                                                                                   new FindMatch(WebServices.GetMatchMakingServices()),
                                                                                                   new GetPlayerSessions(WebServices.GetGameSessionServices(), RepoLocator.GetUserSessionRepo()),
                                                                                                   new CheckMatchFinished(RepoLocator.GetGameSessionRepo()),
                                                                                                   new CheckPlayerTurn(RepoLocator.GetGameSessionRepo(),RepoLocator.GetUserSessionRepo()),
                                                                                                   new GetLoggedPlayer(RepoLocator.GetUserSessionRepo()),
                                                                                                   _sessionButtonClickEvent);
            _findMatchButton.onClick.AddListener(OnFindMatchButtonClick);
            _refreshMatchesButton.onClick.AddListener(OnRefreshMatchesButtonClick);

            _findMatchButton.interactable = true;
            _refreshMatchesButton.interactable = true;
            UpdateMatchScreenParameters();

            isPolling = true;
            StartCoroutine(PollSessionData());
        }

        private void OnDisable()
        {
            _findMatchButton.onClick.RemoveListener(OnFindMatchButtonClick);
            _refreshMatchesButton.onClick.RemoveListener(OnRefreshMatchesButtonClick);
        }

        private void OnRefreshMatchesButtonClick() //Legacy
        {
            OnRefreshMatchesButtonClickEvent.Invoke();
        }

        public void NextScreen()
        {
            isPolling = false;
            StartCoroutine(SwitchScreen(_nextScreen));
        }

        public IEnumerator SwitchScreen(GameObject screen)
        {
            OnSwitchScreenEvent.Invoke();
            _findMatchButton.interactable = false;
            _refreshMatchesButton.interactable = false;

            yield return new WaitForSeconds(2f);
            screen.SetActive(true);
            gameObject.SetActive(false);
        }

        private void InitMatchScreenParameters()
        {
            OnMatchupScreenInitEvent.Invoke();
            OnPlayerNameUpdateEvent.Invoke();
            OnPlayerWinsUpdateEvent.Invoke();
        }

        private void UpdateMatchScreenParameters()
        {
            OnMatchupScreenUpdateEvent.Invoke();
            OnPlayerWinsUpdateEvent.Invoke();
        }

        public void UpdateUserName(string username)
        {
            _userTitleName.text = username;
        }

        public void OnFindMatchButtonClick() => OnFindMatchButtonClickEvent.Invoke();

        public void ShowResultScreen()
        {
            StartCoroutine(SwitchScreen(_resultScreen));
        }

        public void UpdateWinsAmount(int winsAmount)
        {
            _playerWinsText.text = winsAmount.ToString();
        }

        public void EnableFindMatchButton(bool enable)
        {
            _findMatchButton.gameObject.SetActive(enable);
        }

        public void ShowRoundResultsScreen()
        {
            StartCoroutine(SwitchScreen(_roundResultsScreen));
        }

        public void InitSessionContainer(int sessionID, int player1Score, int player2Score, string name1, string name2, bool finished, bool isMyTurn)
        {
            var sessionData = Instantiate(_sessionPrefab, _sessionListContent) as GameObject;
            sessionData.transform.SetAsFirstSibling();

            SessionDataView sessionDataView;

            if (sessionData.TryGetComponent(out sessionDataView))
            {
                _sessionPanels.Add(sessionID, sessionData);

                sessionDataView.SetSessionNames(name1, name2);
                sessionDataView.SetSessionScores(player1Score.ToString(), player2Score.ToString());
                sessionDataView.SetSessionID(sessionID);
                sessionDataView.OnShowResultClickEvent += (sessionID) => OnShowMatchResults.Invoke(sessionID);
                sessionDataView.OnResumeMatchButtonClickEvent += (sessionID) => OnResumeMatch.Invoke(sessionID);

                if (finished)
                    sessionDataView.SetFinished(finished);
                else
                    sessionDataView.SetMyTurn(isMyTurn);
            }
        }

        public void UpdateSessionContainer(int sessionID, int player1Score, int player2Score, string name1, string name2, bool finished, bool isMyTurn)
        {
            if (_sessionPanels.TryGetValue(sessionID, out var sessionData))
            {
                SessionDataView sessionDataView;

                if (sessionData.TryGetComponent(out sessionDataView))
                {
                    sessionDataView.SetSessionScores(player1Score.ToString(), player2Score.ToString());

                    if (finished)
                        sessionDataView.SetFinished(finished);
                    else
                        sessionDataView.SetMyTurn(isMyTurn);

                    sessionDataView.OnShowResultClickEvent -= (sessionID) => OnShowMatchResults.Invoke(sessionID);
                    sessionDataView.OnResumeMatchButtonClickEvent -= (sessionID) => OnResumeMatch.Invoke(sessionID);
                }
            }
            else
            {
                InitSessionContainer(sessionID, player1Score, player2Score, name1, name2, finished, isMyTurn);
            }
        }

        public IEnumerator PollSessionData()
        {
            while (isPolling)
            {
                yield return new WaitForSeconds(3f);
                OnMatchupScreenUpdateEvent?.Invoke();
            }
        }

        public void ResetPanels()
        {
            if (_sessionPanels.Count == 0)
                return;

            SessionDataView sessionDataView;

            foreach (var panel in _sessionPanels)
            {
                if (panel.Value.TryGetComponent(out sessionDataView))
                {
                    sessionDataView.OnShowResultClickEvent -= (sessionID) => OnShowMatchResults.Invoke(sessionID);
                    sessionDataView.OnResumeMatchButtonClickEvent -= (sessionID) => OnResumeMatch.Invoke(sessionID);
                }
                Destroy(panel.Value);
            }
            _sessionPanels.Clear();
        }

        public void EnableLastGameData()
        {
        }

        public void MatchListCanvasGroupToggle(bool active)
        {
            _matchesListCanvasGroup.interactable = active;
        }
    }
}