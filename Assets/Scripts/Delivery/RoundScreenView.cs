using Presentation;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoundScreenView : MonoBehaviour, IRoundScreenView
{
    [SerializeField] private TMP_Text _roundNumber;
    [SerializeField] private TMP_Text _roundLetter;
    [SerializeField] private List<TMP_Text> _categoriesTitles;
    [SerializeField] private List<TMP_InputField> _answerList;
    [SerializeField] private TMP_Text _timer;
    [SerializeField] private Button _stop;
    [SerializeField] private Button _nextRound;

    [SerializeField] private Sprite _correctSprite;
    [SerializeField] private Sprite _incorrectSprite;
    [SerializeField] private List<Image> _results;

    [SerializeField] private GameObject _matchScreen;
    [SerializeField] private GameObject _roundResultScreen;

    public event Action OnInitRound;
    public event Action OnContinueButtonClickEvent;
    public event Action<List<string>> OnStopButtonClickEvent;
    public event Action<List<string>> OnTimerFinished;

    private RoundScreenPresenter _roundScreenPresenter;


    void Start()
    {

    }

    private void OnEnable()
    {
        _roundScreenPresenter = ScreenPresenterBuilder.BuildRoundScreenPresenter(this);

        _stop.onClick.AddListener(OnStopButtonClick);
        _nextRound.onClick.AddListener(OnContinueButtonClick);

        InitiRoundParameters();
        HideResults();
        EnableInputFields();
        StartCoroutine(RoundTimer());

        _stop.gameObject.SetActive(true);
        _nextRound.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        CleanCategories();
        _stop.onClick.RemoveListener(OnStopButtonClick);
        _nextRound.onClick.RemoveListener(OnContinueButtonClick);
    }

    public void BackToMatchScreen()
    {
        StartCoroutine(SwitchScreen(_matchScreen));
    }

    public void ShowRoundResults()
    {
        StartCoroutine(SwitchScreen(_roundResultScreen));
    }

    public IEnumerator SwitchScreen(GameObject nextScreen)
    {
        _nextRound.interactable = false;
        yield return new WaitForSeconds(2f);
        nextScreen.SetActive(true);
        this.gameObject.SetActive(false);
    }

    private void HideResults()
    {
        foreach (Image result in _results)
        {
            //Color transparent = result.color;
            //transparent.a = 0f;
            //result.color = transparent;
            result.enabled = false;
        }    
    }

    private IEnumerator RoundTimer()
    {
        bool finish = false;
        int timerCount = 45;

        while (!finish)
        {
            _timer.text = timerCount.ToString();
            yield return new WaitForSeconds(1f);
            timerCount--;
            if(timerCount == 0)
            {
                finish = true;
            }
        }

        _timer.text = timerCount.ToString();

        OnTimerFinished.Invoke(GetAnswerStrings());
        DisableInputFields();

    }

    private List<string> GetAnswerStrings()
    {
        List<string> answersStrings = new List<string>();
        foreach (var answer in _answerList)
        {
            answersStrings.Add(answer.text);
        }
        return answersStrings;
    }

    private void InitiRoundParameters()
    {
        OnInitRound.Invoke();
    }

    private void OnContinueButtonClick()
    {
        OnContinueButtonClickEvent.Invoke();
    }

    private void OnStopButtonClick()
    {
        StopAllCoroutines();
        _timer.text = "0";
        OnStopButtonClickEvent.Invoke(GetAnswerStrings());
        DisableInputFields();
    }

    public void UpdateRoundNumber(int roundNumber)
    {
        _roundNumber.text = roundNumber.ToString();
    }

    public void UpdateRoundLetter(char roundLetter)
    {
        _roundLetter.text = roundLetter.ToString();
    }

    public void UpdateCategories(List<string> categoryNames)
    {
        int categoryIndex = 0;
        foreach (var category in categoryNames)
        {
            _categoriesTitles[categoryIndex].text = category;
            categoryIndex++;
        }
    }

    public void UpdateResult(int index, bool result)
    {
        ShowResults();

        if (result)
            _results[index].sprite = _correctSprite;
        else
            _results[index].sprite = _incorrectSprite;
    }

    public void EnableNextRoundButton()
    {
        _nextRound.gameObject.SetActive(true);
        _nextRound.interactable = true;
    }

    public void DisableStopButton()
    {
        _stop.gameObject.SetActive(false);
    }

    private void ShowResults()
    {
        foreach (Image resultColor in _results)
        {
            resultColor.enabled = true;
        }
    }

    public void CleanCategories()
    {
        foreach(var answer in _answerList)
        {
            answer.text = String.Empty;
        }
    }

    public void DisableInputFields()
    {
        foreach (var answer in _answerList)
        {
            answer.enabled = false;
        }
    }

    public void EnableInputFields()
    {
        foreach (var answer in _answerList)
        {
            answer.enabled = true;
        }
    }
}
