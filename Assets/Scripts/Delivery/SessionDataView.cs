﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using Reactive;
using UniRx;

public class SessionDataView : MonoBehaviour, ISessionDataView
{
    [SerializeField] private TMP_Text _p1SessionName;
    [SerializeField] private TMP_Text _p2SessionName;
    [SerializeField] private TMP_Text _p1Score;
    [SerializeField] private TMP_Text _p2Score;
    [SerializeField] private TMP_Text _sessionIDText;
    [SerializeField] private Button _resumeGameButton;
    [SerializeField] private Button _showResultButton;

    [SerializeField] private UpdatePlayerScoreEvent _updateSessionEvent;
    [SerializeField] private SessionButtonClickEvent _sessionButtonClickEvent;

    public int _sessionID;

    public event Action OnSessionDataScreenInitEvent;
    public event Action<int> OnResumeMatchButtonClickEvent;
    public event Action<int> OnShowResultClickEvent;
    public event Action OnSwitchScreenEvent;


    void Start()
    {
        _sessionButtonClickEvent.sessionButtonClicked.Subscribe(sessionButtonClicked =>{ if (sessionButtonClicked) { DisableButtons(); } else { EnableButtons(); } }).AddTo(this);
    }

    private void OnEnable()
    {
        _resumeGameButton.onClick.AddListener(OnResumeMatchButtonClick);
        _showResultButton.onClick.AddListener(OnShowResultButtonClick);
        _showResultButton.gameObject.SetActive(false);
        _resumeGameButton.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        _sessionButtonClickEvent.sessionButtonClicked.Dispose();
        _resumeGameButton.onClick.RemoveListener(OnResumeMatchButtonClick);
        _showResultButton.onClick.RemoveListener(OnShowResultButtonClick);
    }

    private void DisableButtons()
    {
        //_resumeGameButton.interactable = false;
        //_showResultButton.interactable = false;
    }

    private void EnableButtons()
    {
        _resumeGameButton.interactable = true;
        _showResultButton.interactable = true;
    }

    private void OnShowResultButtonClick()
    {
        OnShowResultClickEvent.Invoke(_sessionID);
    }

    private void OnResumeMatchButtonClick()
    {
        OnResumeMatchButtonClickEvent.Invoke(_sessionID);
    }

    public void SetSessionNames(string player1, string player2)
    {
        _p1SessionName.text = player1;
        _p2SessionName.text = player2;
    }

    public void SetSessionScores(string p1Score, string p2Score)
    {
        _p1Score.text = p1Score;
        _p2Score.text = p2Score;
    }

    public void SetSessionID(int sessionID)
    {
        _sessionID = sessionID;
        _sessionIDText.text = sessionID.ToString();
    }

    public void SetFinished(bool finished)
    {
        _showResultButton.gameObject.SetActive(finished);
        _resumeGameButton.gameObject.SetActive(false);
    }

    public void SetMyTurn(bool isMyTurn)
    {
        _resumeGameButton.gameObject.SetActive(isMyTurn);
    }
}

