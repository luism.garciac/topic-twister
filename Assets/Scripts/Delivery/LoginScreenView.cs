using Actions;
using Presentation;
using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Delivery
{
    public class LoginScreenView : MonoBehaviour, ILoginScreenView
    {
        [SerializeField] private Button _loginButton;
        [SerializeField] private TMP_InputField _userIDInputField;
        [SerializeField] private GameObject _nextScreen;
        [SerializeField] private AdsManager _adsManager;

        private LoginScreenPresenter _loginScreenViewPresenter;

        public event Action<string> OnLoginButtonClickEvent;

        private void Start()
        {
            _loginScreenViewPresenter = ScreenPresenterBuilder.BuildLoginScreenPresenter(this, new Login(RepoLocator.GetUserSessionRepo(),
                                                                                                         WebServices.GetLoginServices()));
            _loginButton.onClick.AddListener(OnLoginButtonClick);
        }

        private void OnDisable()
        {
            _loginButton.onClick.RemoveListener(OnLoginButtonClick);
        }

        public void OnLoginButtonClick()
        {
            OnLoginButtonClickEvent.Invoke(_userIDInputField.text);
        }

        public void NextScreen()
        {
            _adsManager.ShowTestAd();
            StartCoroutine(SwitchScreen());
        }

        public IEnumerator SwitchScreen()
        {
            _loginButton.interactable = false;
            yield return new WaitForSeconds(2f);

            _nextScreen.SetActive(true);
            gameObject.SetActive(false);
        }

        public void ShowPopUp(string text)
        {
            var popUpInstance = Instantiate(Resources.Load("Prefabs/PopUp"), transform) as GameObject;
            PopUp popUp;

            if (popUpInstance.TryGetComponent(out popUp))
            {
                popUp.SetText(text);
                popUp.OnCloseButton += ShowCanvasGroup;
                ShowCanvasGroup(false);
            }
        }

        public void ShowCanvasGroup(bool show)
        {
            GetComponentInChildren<CanvasGroup>().interactable = show;
        }
    }
}