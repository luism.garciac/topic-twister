using Presentation;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoundResultScreenView : MonoBehaviour, IRoundResultScreenView
{
    [SerializeField] private TMP_Text _roundNumber;
    [SerializeField] private TMP_Text _roundLetter;

    [SerializeField] private TMP_Text _player1Name;
    [SerializeField] private TMP_Text _player2Name;

    [SerializeField] private List<TMP_Text> _categoriesTitles;
    [SerializeField] private List<TMP_InputField> _playerAnswerList;
    [SerializeField] private List<TMP_InputField> _opponentAnswerList;

    [SerializeField] private Button _nextButton;

    [SerializeField] private Sprite _correctSprite;
    [SerializeField] private Sprite _incorrectSprite;
    [SerializeField] private List<Image> _playerResults;
    [SerializeField] private List<Image> _opponentResults;

    [SerializeField] private GameObject _matchScreen;
    [SerializeField] private GameObject _resultScreen;


    public event Action OnInitRoundResults;
    public event Action OnContinueButtonClickEvent;

    private RoundResultsScreenPresenter _roundResultsScreenPresenter;


    void Start()
    {

    }

    private void OnEnable()
    {
        _roundResultsScreenPresenter = ScreenPresenterBuilder.BuildRoundResultScreenPresenter(this);

        _nextButton.onClick.AddListener(OnContinueButtonClick);
        _nextButton.interactable = true;

        InitiRoundResultsParameters();
    }

    private void OnDisable()
    {
        _nextButton.onClick.RemoveListener(OnContinueButtonClick);
        CleanCategories();
    }

    public void BackToMatchScreen()
    {
        StartCoroutine(SwitchScreen(_matchScreen));
    }

    public IEnumerator SwitchScreen(GameObject screen)
    {
        _nextButton.interactable = false;
        yield return new WaitForSeconds(2f);
        screen.SetActive(true);
        this.gameObject.SetActive(false);
    }

    private void InitiRoundResultsParameters()
    {
        OnInitRoundResults.Invoke();
    }

    private void OnContinueButtonClick()
    {
        OnContinueButtonClickEvent.Invoke();
    }

    public void UpdateRoundNumber(int roundNumber)
    {
        _roundNumber.text = roundNumber.ToString();
    }

    public void UpdateRoundLetter(char roundLetter)
    {
        _roundLetter.text = roundLetter.ToString();
    }

    public void UpdateCategories(List<string> categoryNames)
    {
        int categoryIndex = 0;
        foreach (var category in categoryNames)
        {
            _categoriesTitles[categoryIndex].text = category;
            categoryIndex++;
        }
    }

    public void UpdatePlayerAnswers(List<string> playerAnswers, string playerName)
    {
        int answerIndex = 0;
        foreach (var answer in playerAnswers)
        {
            _playerAnswerList[answerIndex].text = answer;
            answerIndex++;
        }

        _player1Name.text = playerName;
    }

    public void UpdateOpponentAnswers(List<string> opponentAnswers, string opponentName)
    {
        int answerIndex = 0;
        foreach (var answer in opponentAnswers)
        {
            _opponentAnswerList[answerIndex].text = answer;
            answerIndex++;
        }

        _player2Name.text = opponentName;
    }

    public void UpdatePlayerResults(int index, bool result)
    {
        if (result)
            _playerResults[index].sprite = _correctSprite;
        else
            _playerResults[index].sprite = _incorrectSprite;
    }

    public void UpdateOpponentResults(int index, bool result)
    {
        if (result)
            _opponentResults[index].sprite = _correctSprite;
        else
            _opponentResults[index].sprite = _incorrectSprite;
    }

    public void CleanCategories()
    {
        foreach(var answer in _playerAnswerList)
        {
            answer.text = String.Empty;
        }

        foreach (var answer in _opponentAnswerList)
        {
            answer.text = String.Empty;
        }
    }

    public void ShowResultScreen()
    {
        StartCoroutine(SwitchScreen(_resultScreen));
    }
}
