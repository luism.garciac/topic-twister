using Actions;
using Presentation;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RollScreenView : MonoBehaviour , IRollScreenView
{

    [SerializeField] private TMP_Text _roundNumber;
    [SerializeField] private TMP_Text _opponentName;
    [SerializeField] private List<TMP_Text> _categories;
    [SerializeField] private TMP_Text _roundLetter;
    [SerializeField] private Button _rollButton;
    [SerializeField] private GameObject _nextScreen;

    private RollScreenPresenter _rollScreenViewPresenter;

    public event Action OnRollButtonClickEvent;
    public event Action OnInitRollScreenEvent;
    public event Action OnSwitchScreenEvent;
    public event Action OnUpdateRoundNumberEvent;
    public event Action OnUpdateOpponentNameEvent;
    public event Action OnUpdateCategoryNames;

    private void Start()
    {
    }

    private void OnEnable()
    {
        _rollScreenViewPresenter = ScreenPresenterBuilder.BuildRollScreenPresenter(this, 
                                                                                   new GetRoundCategoryNames(RepoLocator.GetGameSessionRepo()),
                                                                                   new GetSessionOpponent(RepoLocator.GetGameSessionRepo(), RepoLocator.GetUserSessionRepo()),
                                                                                   new GetCurrentRound(RepoLocator.GetGameSessionRepo()));
        _rollButton.onClick.AddListener(OnRollButtonClick);
        InitRollScreen();
    }

    private void OnDisable()
    {
        _rollButton.onClick.RemoveListener(OnRollButtonClick);
    }

    private void InitRollScreen()
    {
        _rollButton.interactable = true;
        OnInitRollScreenEvent.Invoke();
    }

    public void InitViewElements()
    {
        OnUpdateRoundNumberEvent.Invoke();
        OnUpdateOpponentNameEvent.Invoke();
        OnUpdateCategoryNames.Invoke();
    }

    public virtual void UpdateRoundNumber(int roundNumber)
    {
        _roundNumber.text = roundNumber.ToString();
    }

    public virtual void UpdateOpponentName(string opponentName)
    {
        _opponentName.text = opponentName;
    }

    public void OnRollButtonClick()
    {
        OnRollButtonClickEvent.Invoke();
    }

    public virtual void UpdateCategories(List<string> categoryNames)
    {
        int categoryIndex = 0;
        foreach (var category in categoryNames)
        {
            _categories[categoryIndex].text = category;
            categoryIndex++;
        }
    }

    public void UpdateRoundLetter(char roundLetter)
    {
        StartCoroutine(RollLetterProcedure(roundLetter));
    }

    private IEnumerator RollLetterProcedure(char roundLetter)
    {
        _rollButton.interactable = false;

        float rollingTime = 0f;
        while (rollingTime < 3f)
        {
            _roundLetter.text = ((char)UnityEngine.Random.Range(65, 91)).ToString();
            yield return new WaitForSeconds(0.1f);
            rollingTime += 0.1f;
        }

        _roundLetter.text = roundLetter.ToString();

        NextScreen();
    }

    private void NextScreen()
    {
        StartCoroutine(SwitchScreen());
    }

    private IEnumerator SwitchScreen()
    {
        yield return new WaitForSeconds(2f);

        OnSwitchScreenEvent.Invoke();
        _nextScreen.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
