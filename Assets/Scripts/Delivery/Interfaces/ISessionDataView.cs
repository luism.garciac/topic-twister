﻿using System;
using UnityEngine.Events;

public interface ISessionDataView
{
    public event Action OnSessionDataScreenInitEvent;
    public event Action<int>OnResumeMatchButtonClickEvent;
    public event Action<int> OnShowResultClickEvent;
    public event Action OnSwitchScreenEvent;
    public void SetFinished(bool finished);
    public void SetMyTurn(bool isMyTurn);
}
