﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public interface IResultScreenView
{
    public event Action OnResultScreenInitEvent;
    public event Action OnEndMatchButtonClickEvent;
    public void NextScreen();
    public void UpdateWinner(string winner);
}

