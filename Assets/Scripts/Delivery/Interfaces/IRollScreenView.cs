﻿using System;
using System.Collections.Generic;
using UnityEngine.Events;

public interface IRollScreenView
{
    public event Action OnRollButtonClickEvent;
    public event Action OnUpdateRoundNumberEvent;
    public event Action OnUpdateOpponentNameEvent;
    public event Action OnUpdateCategoryNames;
    public event Action OnInitRollScreenEvent;
    public event Action OnSwitchScreenEvent;

    public void UpdateRoundNumber(int roundNumber);
    public void UpdateRoundLetter(char roundLetter);
    public void UpdateCategories(List<string> categoryNames);
    public void UpdateOpponentName(string opponentName);
    public void InitViewElements();
}

