﻿using System;

public interface ILoginScreenView
{
    public event Action<string> OnLoginButtonClickEvent;
    public void NextScreen();
    public void ShowPopUp(string text);
    public void ShowCanvasGroup(bool show);
}

