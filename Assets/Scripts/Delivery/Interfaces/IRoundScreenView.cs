﻿using System;
using System.Collections.Generic;
using UnityEngine.Events;

public interface IRoundScreenView
{
    public event Action OnInitRound;
    public event Action OnContinueButtonClickEvent;

    public event Action<List<string>> OnStopButtonClickEvent;
    public event Action<List<string>> OnTimerFinished;

    public void UpdateRoundNumber(int roundNumber);
    public void UpdateRoundLetter(char roundLetter);
    public void UpdateCategories(List<string> categoryNames);
    public void UpdateResult(int index, bool result);
    public void EnableNextRoundButton();
    public void DisableStopButton();
    public void BackToMatchScreen();
    public void ShowRoundResults();
    public void CleanCategories();
    public void DisableInputFields();
    public void EnableInputFields();
}
