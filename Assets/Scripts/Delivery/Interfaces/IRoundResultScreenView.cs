﻿using System;
using System.Collections.Generic;
using UnityEngine.Events;

public interface IRoundResultScreenView
{
    public event Action OnInitRoundResults;
    public event Action OnContinueButtonClickEvent;
    public void UpdateRoundNumber(int roundNumber);
    public void UpdateRoundLetter(char roundLetter);
    public void UpdateCategories(List<string> categoryNames);
    public void UpdatePlayerAnswers(List<string> playerAnswers, string playerName);
    public void UpdateOpponentAnswers(List<string> opponentAnswers, string opponentName);
    public void UpdatePlayerResults(int index, bool result);
    public void UpdateOpponentResults(int index, bool result);
    public void BackToMatchScreen();
    public void CleanCategories();
    public void ShowResultScreen();
}
