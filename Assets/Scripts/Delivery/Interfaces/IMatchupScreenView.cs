﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public interface IMatchupScreenView
{
    public event Action OnMatchupScreenInitEvent;
    public event Action OnMatchupScreenUpdateEvent;
    public event Action OnPlayerNameUpdateEvent;
    public event Action OnPlayerWinsUpdateEvent;
    public event Action OnResumeMatchButtonClickEvent;
    public event Action OnFindMatchButtonClickEvent;
    public event Action OnRefreshMatchesButtonClickEvent;
    public event Action<int> OnShowMatchResults;
    public event Action<int> OnResumeMatch;
    public event Action OnSwitchScreenEvent;


    public void NextScreen();

    public void UpdateUserName(string username);
    public void EnableLastGameData();
    void ShowResultScreen();
    void UpdateWinsAmount(int winsAmount);
    void EnableFindMatchButton(bool enable);
    void ShowRoundResultsScreen();
    void InitSessionContainer(int sessionID, int player1Score, int player2Score, string name1, string name2, bool finished, bool isMyTurn);
    void UpdateSessionContainer(int sessionID, int player1Score, int player2Score, string name1, string name2, bool finished, bool isMyTurn);
    void ResetPanels();
    void MatchListCanvasGroupToggle(bool active);
}

