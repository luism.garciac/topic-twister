﻿using Actions;
using Delivery;
using Reactive;

namespace Presentation
{
    public class ScreenPresenterBuilder
    {
        public static RollScreenPresenter BuildRollScreenPresenter(RollScreenView view,
                                                                   GetRoundCategoryNames getRoundCategories,
                                                                   GetSessionOpponent getSessionOpponent,
                                                                   GetCurrentRound getCurrentRound)
        {
            return new RollScreenPresenter(view, getRoundCategories, getSessionOpponent, getCurrentRound);
        }

        public static RoundScreenPresenter BuildRoundScreenPresenter(IRoundScreenView view)
        {
            return new RoundScreenPresenter(view);
        }

        public static RoundResultsScreenPresenter BuildRoundResultScreenPresenter(IRoundResultScreenView view)
        {
            return new RoundResultsScreenPresenter(view);
        }

        public static LoginScreenPresenter BuildLoginScreenPresenter(LoginScreenView view, Login login)
        {
            return new LoginScreenPresenter(view, login);
        }

        public static MatchupScreenPresenter BuildMatchupScreenPresenter(MatchupScreenView view, 
                                                                         GetPlayerInfo getPlayerInfo, 
                                                                         FindMatch findMatch, 
                                                                         GetPlayerSessions getPlayerGameSessions, 
                                                                         CheckMatchFinished checkMatchFinished, 
                                                                         CheckPlayerTurn checkPlayerTurn, 
                                                                         GetLoggedPlayer getLoggedPlayer,
                                                                         SessionButtonClickEvent sessionButtonClickEvent)
        {
            return new MatchupScreenPresenter(view, getPlayerInfo, findMatch, getPlayerGameSessions, checkMatchFinished, checkPlayerTurn, getLoggedPlayer, sessionButtonClickEvent);
        }

        public static ResultScreenPresenter BuildResultScreenPresenter(ResultScreenView view)
        {
            return new ResultScreenPresenter(view);
        }

        public static SessionDataPresenter BuildSessionDataPresenter(SessionDataView view)
        {
            return new SessionDataPresenter(view);
        }
    }
}