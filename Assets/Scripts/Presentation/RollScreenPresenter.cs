﻿using Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class RollScreenPresenter
{
    private IRollScreenView _rollScreenView;
    private GetRoundCategoryNames _getRoundCategories;
    private GetSessionOpponent _getSessionOpponent;
    private GetCurrentRound _getCurrentRound;
    private Round _currentRound;
    private char _testLetter = 'A';

    public RollScreenPresenter(IRollScreenView rollScreenView,
                               GetRoundCategoryNames getRoundCategories,
                               GetSessionOpponent getSessionOpponent,
                               GetCurrentRound getCurrentRound)
    {
        _rollScreenView = rollScreenView;
        _rollScreenView.OnRollButtonClickEvent += Roll;
        _rollScreenView.OnInitRollScreenEvent += InitRound;
        _rollScreenView.OnSwitchScreenEvent += OnSwitchScreen;
        _rollScreenView.OnUpdateRoundNumberEvent += OnUpdateRoundNumber;
        _rollScreenView.OnUpdateOpponentNameEvent += OnUpdateOpponentName;
        _rollScreenView.OnUpdateCategoryNames += OnUpdateCategoryNames;

        _getRoundCategories = getRoundCategories;
        _getSessionOpponent = getSessionOpponent;
        _getCurrentRound = getCurrentRound;
    }

    private void OnUpdateCategoryNames()
    {
        _rollScreenView.UpdateCategories(_getRoundCategories.Execute());
    }

    private void OnUpdateOpponentName()
    {
        _rollScreenView.UpdateOpponentName(_getSessionOpponent.Execute().PlayerData.Name);
    }

    private void OnSwitchScreen()
    {
        _rollScreenView.OnRollButtonClickEvent -= Roll;
        _rollScreenView.OnInitRollScreenEvent -= InitRound;
        _rollScreenView.OnSwitchScreenEvent -= OnSwitchScreen;
        _rollScreenView.OnUpdateRoundNumberEvent -= OnUpdateRoundNumber;
        _rollScreenView.OnUpdateOpponentNameEvent -= OnUpdateOpponentName;
        _rollScreenView.OnUpdateCategoryNames -= OnUpdateCategoryNames;
    }

    private void InitRound()
    {
        _currentRound = _getCurrentRound.Execute();
        _rollScreenView.InitViewElements();
    }

    private void OnUpdateRoundNumber()
    {
        _rollScreenView.UpdateRoundNumber(_currentRound.RoundNumber);
    }

    public void SetCategoryNames(List<string> categoryNames)
    {
        _rollScreenView.UpdateCategories(categoryNames);
    }

    private void Roll()
    {
        _rollScreenView.UpdateRoundLetter(_currentRound.RoundLetter);
    }
}
