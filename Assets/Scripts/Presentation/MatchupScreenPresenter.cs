﻿using Reactive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using Actions;

public class MatchupScreenPresenter
{
    private IMatchupScreenView _matchupScreenView;

    private GetPlayerInfo _getPlayerInfo;
    private FindMatch _findMatch;
    private GetPlayerSessions _getPlayerGameSessions;
    private CheckMatchFinished _checkMatchFinished;
    private CheckPlayerTurn _checkPlayerTurn;
    private GetLoggedPlayer _getLoggedPlayer;

    private SessionButtonClickEvent _sessionButtonClickEvent;

    public MatchupScreenPresenter(IMatchupScreenView matchupScreenView, 
                                  GetPlayerInfo getPlayerInfo, 
                                  FindMatch findMatch, 
                                  GetPlayerSessions getPlayerGameSessions, 
                                  CheckMatchFinished checkMatchFinished, 
                                  CheckPlayerTurn checkPlayerTurn,
                                  GetLoggedPlayer loggedPlayer,
                                  SessionButtonClickEvent sessionButtonClickEvent)
    {
        _matchupScreenView = matchupScreenView;
        _getPlayerInfo = getPlayerInfo;
        _findMatch = findMatch;
        _sessionButtonClickEvent = sessionButtonClickEvent;
        _getPlayerGameSessions = getPlayerGameSessions;
        _checkMatchFinished = checkMatchFinished;
        _checkPlayerTurn = checkPlayerTurn;
        _getLoggedPlayer = loggedPlayer;

        _matchupScreenView.OnFindMatchButtonClickEvent += FindMatch;
        _matchupScreenView.OnRefreshMatchesButtonClickEvent += InitMatchParameters;
        _matchupScreenView.OnMatchupScreenInitEvent += InitMatchParameters;
        _matchupScreenView.OnMatchupScreenUpdateEvent += UpdateMatchParameters;
        _matchupScreenView.OnResumeMatch += ResumeMatch;
        _matchupScreenView.OnShowMatchResults += ShowMatchResult;
        _matchupScreenView.OnSwitchScreenEvent += OnSwitchScreen;
        _matchupScreenView.OnPlayerNameUpdateEvent += OnTitlePlayerNameUpdate;
        _matchupScreenView.OnPlayerWinsUpdateEvent += OnPlayerWinsUpdate;
    }
    private void OnSwitchScreen()
    {
        _matchupScreenView.OnFindMatchButtonClickEvent -= FindMatch;
        _matchupScreenView.OnRefreshMatchesButtonClickEvent -= InitMatchParameters;
        _matchupScreenView.OnMatchupScreenInitEvent -= InitMatchParameters;
        _matchupScreenView.OnMatchupScreenUpdateEvent -= UpdateMatchParameters;
        _matchupScreenView.OnResumeMatch -= ResumeMatch;
        _matchupScreenView.OnShowMatchResults -= ShowMatchResult;
        _matchupScreenView.OnSwitchScreenEvent -= OnSwitchScreen;
        _matchupScreenView.OnPlayerNameUpdateEvent -= OnTitlePlayerNameUpdate;
        _matchupScreenView.OnPlayerWinsUpdateEvent -= OnPlayerWinsUpdate;
    }

    private async void InitMatchParameters()
    {
        try
        {
            OnTitlePlayerNameUpdate();
            OnPlayerWinsUpdate();

            await InitSessionList();
        }
        catch (Exception e)
        {
            Debug.Log($"No sessions Open: {e.Message}");
        }
    }

    private async Task InitSessionList()
    {
        List<GameSession> gameSessions = await _getPlayerGameSessions.Execute();
        RepoLocator.GetGameSessionRepo().SetGameSessions(gameSessions);

        _matchupScreenView.ResetPanels();

        var sortedGameSessions = gameSessions.OrderBy(session => session.SessionID);

        foreach (GameSession session in sortedGameSessions)
        {
            bool finished = _checkMatchFinished.Execute(session.SessionID);
            bool isMyTurn = _checkPlayerTurn.Execute(session.SessionID);

            _matchupScreenView.InitSessionContainer(session.SessionID, session.Player1Score, session.Player2Score,
                                                    session.Player1.PlayerData.Name, session.Player2.PlayerData.Name,
                                                    finished, isMyTurn);
        }
    }

    private async void UpdateMatchParameters()
    {
        List<GameSession> gameSessions = await _getPlayerGameSessions.Execute();
        RepoLocator.GetGameSessionRepo().SetGameSessions(gameSessions);

        var sortedGameSessions = gameSessions.OrderBy(session => session.SessionID);

        foreach (GameSession session in sortedGameSessions)
        {
            bool finished = _checkMatchFinished.Execute(session.SessionID);
            bool isMyTurn = _checkPlayerTurn.Execute(session.SessionID);

            _matchupScreenView.UpdateSessionContainer(session.SessionID, session.Player1Score, session.Player2Score, session.Player1.PlayerData.Name, session.Player2.PlayerData.Name, finished, isMyTurn);
        }
    }

    private async void OnTitlePlayerNameUpdate()
    {
        Player player = await _getPlayerInfo.Execute();
        string userName = player.PlayerData.Name;
        _matchupScreenView.UpdateUserName(userName);
    }

    private async void OnPlayerWinsUpdate()
    {
        Player player = await _getPlayerInfo.Execute();
        int winsAmount = player.PlayerData.WinsAmount;
        _matchupScreenView.UpdateWinsAmount(winsAmount);
    }

    private async void FindMatch()
    {
        Player player = _getLoggedPlayer.Execute();

        GameSession newGameSession = await _findMatch.Execute(player);

        if (newGameSession != null)
        {
            RepoLocator.GetGameSessionRepo().SetCurrentGameSession(newGameSession);
            _matchupScreenView.NextScreen();
        }
        else
        {
            Debug.LogError("No Match Found");
        }
    }

    private void ShowMatchResult(int sessionID)
    {
        RepoLocator.GetGameSessionRepo().GetGameSessionByID(sessionID);
        _matchupScreenView.ShowResultScreen();
        _sessionButtonClickEvent.sessionButtonClicked.Value = true;
    }

    private void ResumeMatch(int sessionID)
    {
        RepoLocator.GetGameSessionRepo().GetGameSessionByID(sessionID);
        _matchupScreenView.NextScreen();
        _sessionButtonClickEvent.sessionButtonClicked.Value = true;
    }
}
