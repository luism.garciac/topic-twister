﻿using Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class RoundResultsScreenPresenter
{

    private IRoundResultScreenView _roundResultsScreenView;
    private Round _lastFinishedRound;

    public RoundResultsScreenPresenter(IRoundResultScreenView roundResultsScreenView)
    {
        _roundResultsScreenView = roundResultsScreenView;
        _roundResultsScreenView.OnInitRoundResults += InitRoundResultsParameters;
        _roundResultsScreenView.OnContinueButtonClickEvent += ContinueGame;
    }

    private void ContinueGame()
    {
        _roundResultsScreenView.OnInitRoundResults -= InitRoundResultsParameters;
        _roundResultsScreenView.OnContinueButtonClickEvent -= ContinueGame;

        if (CheckMatchFinished())
        {
            //SetFinalScores();
            _roundResultsScreenView.ShowResultScreen();
        }
        else
            _roundResultsScreenView.BackToMatchScreen();
    }

    private void InitRoundResultsParameters()
    {
        SetCurrentRound();
        SetCategoryNames();
        SetRoundLetter();
        SetRoundAnswers();
        CheckMatchFinished();
    }

    private void SetRoundAnswers()
    {
        SetPlayerAnswers();
        SetOpponentAnswers();
    }

    private void SetPlayerAnswers()
    {
        string player1Name = RepoLocator.GetGameSessionRepo().GetCurrentGameSession().Player1.PlayerData.Name;

        List<Answer> playerAnswers = _lastFinishedRound.Player1Answers;
        List<string> stringAnswers = GetAnswerStrings(playerAnswers);
        _roundResultsScreenView.UpdatePlayerAnswers(stringAnswers, player1Name);

        for (int answerIndex = 0; answerIndex < playerAnswers.Count; answerIndex++)
        {
            bool answerStatus = playerAnswers[answerIndex].Correct;
            _roundResultsScreenView.UpdatePlayerResults(answerIndex, answerStatus);
        }
    }

    private void SetOpponentAnswers()
    {
        string player2Name = RepoLocator.GetGameSessionRepo().GetCurrentGameSession().Player2.PlayerData.Name;

        List<Answer> opponentAnswers = _lastFinishedRound.Player2Answers;
        List<string> stringAnswers = GetAnswerStrings(opponentAnswers);
        _roundResultsScreenView.UpdateOpponentAnswers(stringAnswers, player2Name);

        for (int answerIndex = 0; answerIndex < opponentAnswers.Count; answerIndex++)
        {
            bool answerStatus = opponentAnswers[answerIndex].Correct;
            _roundResultsScreenView.UpdateOpponentResults(answerIndex, answerStatus);
        }
    }

    private List<string> GetAnswerStrings(List<Answer> playerAnswers)
    {
        List<string> stringAnswers = new List<string>();
        foreach (var playerAnswer in playerAnswers)
        {
            stringAnswers.Add(playerAnswer.PlayerAnswer);
        }
        return stringAnswers;
    }

    public void SetCurrentRound()
    {
        _lastFinishedRound = new GetLastFinishedRound(RepoLocator.GetGameSessionRepo()).Execute();

        _roundResultsScreenView.UpdateRoundNumber(_lastFinishedRound.RoundNumber);
    }

    public void SetCategoryNames()
    {
        GetRoundCategoryNames getRoundCategoryNames = new GetRoundCategoryNames(RepoLocator.GetGameSessionRepo());
        _roundResultsScreenView.UpdateCategories(getRoundCategoryNames.Execute(_lastFinishedRound));
    }

    public void SetRoundLetter()
    {
        _roundResultsScreenView.UpdateRoundLetter(_lastFinishedRound.RoundLetter);
    }

    private bool CheckMatchFinished()
    {
        CheckMatchFinished checkMatchFinished = new CheckMatchFinished(RepoLocator.GetGameSessionRepo());
        return checkMatchFinished.Execute();
    }
}

