﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class ResultScreenPresenter
{

    IResultScreenView _resultScreenView;
    public ResultScreenPresenter(IResultScreenView resultScreenView)
    {
        _resultScreenView = resultScreenView;
        _resultScreenView.OnEndMatchButtonClickEvent += EndMatch;
        _resultScreenView.OnResultScreenInitEvent += InitResultScreen;
    }

    private void InitResultScreen()
    {
        GameSession gameSession = RepoLocator.GetGameSessionRepo().GetCurrentGameSession();
        string winner = String.Empty;

        if (!gameSession.IsTie)
        {
            _resultScreenView.UpdateWinner(gameSession.Winner.PlayerData.Name);
        }
        else
        {
            _resultScreenView.UpdateWinner("Empate");
        }
    }

    private void EndMatch()
    {
        _resultScreenView.OnEndMatchButtonClickEvent -= EndMatch;
        _resultScreenView.OnResultScreenInitEvent -= InitResultScreen;
        _resultScreenView.NextScreen();
    }
}

