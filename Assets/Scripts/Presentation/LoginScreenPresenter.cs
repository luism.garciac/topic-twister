﻿using Actions;
using System;

namespace Presentation
{
    public class LoginScreenPresenter
    {
        private ILoginScreenView _loginScreenView;
        private Login _login;

        public LoginScreenPresenter(ILoginScreenView loginScreenView, Login login)
        {
            _loginScreenView = loginScreenView;
            _login = login;
            _loginScreenView.OnLoginButtonClickEvent += Login;
        }

        public async void Login(string userID)
        {
            _loginScreenView.ShowCanvasGroup(false);

            if (userID == string.Empty)
            {
                _loginScreenView.ShowPopUp("Ingrese Nombre de Usuario");
                return;
            }

            try
            {
                await _login.Execute(userID);
                _loginScreenView.NextScreen();
            }
            catch (Exception)
            {
                _loginScreenView.ShowPopUp("Usuario no encontrado");
            }
        }
    }
}