﻿using Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class RoundScreenPresenter
{

    IRoundScreenView _roundScreenView;

    public RoundScreenPresenter(IRoundScreenView roundScreenView)
    {
        _roundScreenView = roundScreenView;
        _roundScreenView.OnInitRound += InitRoundParameters;
        _roundScreenView.OnStopButtonClickEvent += ValidateAnswers;
        _roundScreenView.OnContinueButtonClickEvent += ContinueGame;
        _roundScreenView.OnTimerFinished += ValidateAnswers;
    }

    private async void ContinueGame()
    {
        int currentSessionID = RepoLocator.GetGameSessionRepo().GetCurrentGameSession().SessionID;
        GameSession updatedGameSession = await WebServices.GetGameSessionServices().GetSessionByID(currentSessionID);
        RepoLocator.GetGameSessionRepo().SetCurrentGameSession(updatedGameSession);
        CheckPlayerTurn checkPlayerTurn = new CheckPlayerTurn(RepoLocator.GetGameSessionRepo(), RepoLocator.GetUserSessionRepo());

        UnsubscribeEvents();

        if (checkPlayerTurn.Execute(updatedGameSession))
        {
            _roundScreenView.ShowRoundResults();
        }
        else
        {
            _roundScreenView.BackToMatchScreen();
        }
    }

    private void UnsubscribeEvents()
    {
        _roundScreenView.OnStopButtonClickEvent -= ValidateAnswers;
        _roundScreenView.OnContinueButtonClickEvent -= ContinueGame;
        _roundScreenView.OnTimerFinished -= ValidateAnswers;
        _roundScreenView.OnInitRound -= InitRoundParameters;
    }

    public async void ValidateAnswers(List<string> answers) //Este metodo deberia ser privado, es pública solo por test
    {
        _roundScreenView.DisableStopButton();

        string loggedUser = RepoLocator.GetUserSessionRepo().GetCurrentUserSession().Player.UserID;
        GameSession currentGameSession = RepoLocator.GetGameSessionRepo().GetCurrentGameSession();
        UserSession userSession = RepoLocator.GetUserSessionRepo().GetCurrentUserSession();

        RepoLocator.GetGameSessionRepo().SaveRoundAnswers(answers, loggedUser);

        GetCurrentRound getCurrentRound = new GetCurrentRound(RepoLocator.GetGameSessionRepo());
        var currentRound = getCurrentRound.Execute();

        var roundCategories = currentRound.Categories;
        //ValidateAnswer validateAnswer;
        bool correctAnswer;

        List<Answer> updatedAnswers = new List<Answer>();

        if (currentGameSession.Player1.UserID == loggedUser)
        {
            updatedAnswers = currentGameSession.CurrentRound.Player1Answers;
        }
        if (currentGameSession.Player2.UserID == loggedUser)
        {
            updatedAnswers = currentGameSession.CurrentRound.Player2Answers;
        }

        List<Answer> validatedAnswers = await WebServices.GetGameSessionServices().UpdateAnswers(userSession.Player.UserID,
                                                           currentGameSession.SessionID,
                                                           updatedAnswers);

        int categoryIndex = 0;

        foreach (var validatedAnswer in validatedAnswers)
        {
            _roundScreenView.UpdateResult(categoryIndex, validatedAnswer.Correct);
            categoryIndex++;
        }

        _roundScreenView.EnableNextRoundButton();
    }

    private void InitRoundParameters()
    {
        SetCurrentRound();
        SetCategoryNames();
        GetRoundLetter();
    }

    public void SetCurrentRound()
    {
        GetCurrentRound getCurrenRound = new GetCurrentRound(RepoLocator.GetGameSessionRepo());
        var currentRound = getCurrenRound.Execute();
        _roundScreenView.UpdateRoundNumber(currentRound.RoundNumber);
    }

    public void SetCategoryNames()
    {
        GetRoundCategoryNames getRoundCategoryNames = new GetRoundCategoryNames(RepoLocator.GetGameSessionRepo());
        _roundScreenView.UpdateCategories(getRoundCategoryNames.Execute());
    }

    public void GetRoundLetter()
    {
        GetRoundLetter getRoundLetter = new GetRoundLetter(RepoLocator.GetGameSessionRepo());
        var currentRoundLetter = getRoundLetter.Execute();
        _roundScreenView.UpdateRoundLetter(currentRoundLetter);
    }
}

