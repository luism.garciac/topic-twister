﻿using System.Threading.Tasks;

namespace Actions
{
    public class GetLoggedPlayer
    {
        private IUserSessionRepo _userSessionRepo;
        public GetLoggedPlayer(IUserSessionRepo userSessionRepo)
        {
            _userSessionRepo = userSessionRepo;
        }

        public virtual Player Execute()
        {
            return _userSessionRepo.GetCurrentUserSession().Player;
        }
    }
}