﻿namespace Actions
{
    public class GetCurrentRound
    {
        IGameSessionRepo _sessionRepo;

        public GetCurrentRound(IGameSessionRepo sessionRepo)
        {
            _sessionRepo = sessionRepo;
        }

        public virtual Round Execute()
        {
            return _sessionRepo.GetCurrentGameSession().CurrentRound;
        }
    }
}