﻿using System.Collections.Generic;

namespace Actions
{
    public class GetLastFinishedRound
    {
        IGameSessionRepo _sessionRepo;
        public GetLastFinishedRound(IGameSessionRepo sessionRepo)
        {
            _sessionRepo = sessionRepo;
        }
        public Round Execute()
        {
            GameSession gameSession = _sessionRepo.GetCurrentGameSession();

            List<Round> matchRounds = gameSession.MatchRounds;

            for (int i = matchRounds.Count - 1; i >= 0; i--)
            {
                if (matchRounds[i].Player1Answers.Count != 0 && matchRounds[i].Player2Answers.Count != 0)
                    return matchRounds[i];
            }

            return null;
        }
    }
}