﻿namespace Actions
{
    public class CheckMatchFinished
    {
        private IGameSessionRepo _sessionRepo;
        public CheckMatchFinished(IGameSessionRepo sessionRepo)
        {
            _sessionRepo = sessionRepo;
        }

        public virtual bool Execute(int sessionID)
        {
            GameSession currentSession = _sessionRepo.GetGameSessionByID(sessionID, false);

            return currentSession.IsTie || currentSession.Winner != null;
        }

        public virtual bool Execute()
        {
            GameSession currentSession = _sessionRepo.GetCurrentGameSession();

            return currentSession.IsTie || currentSession.Winner != null;
        }
    }
}