﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Actions
{
    public class GetPlayerSessions
    {
        private IWebGameSessionServices _webGameSessionRepo;
        private IUserSessionRepo _userSessionRepo;
        public GetPlayerSessions(IWebGameSessionServices webGameSessionRepo, IUserSessionRepo userSessionRepo)
        {
            _webGameSessionRepo = webGameSessionRepo;
            _userSessionRepo = userSessionRepo;
        }

        public virtual async Task<List<GameSession>> Execute()
        {
            return await _webGameSessionRepo.FindOpenGameSessions(_userSessionRepo.GetCurrentUserSession().Player.UserID);
        }
    }
}