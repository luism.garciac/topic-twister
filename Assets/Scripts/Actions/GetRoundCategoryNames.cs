using System.Collections.Generic;

namespace Actions
{
    public class GetRoundCategoryNames

    {
        private IGameSessionRepo _sessionRepo;
        public GetRoundCategoryNames(IGameSessionRepo sessionRepo)
        {
            _sessionRepo = sessionRepo;
        }

        public virtual List<string> Execute()
        {
            var roundCategories = _sessionRepo.GetCurrentGameSession().CurrentRound.Categories;

            List<string> categoryNames = new List<string>();
            foreach (Category category in roundCategories)
            {
                categoryNames.Add(category.CategoryName);
            }

            return categoryNames;
        }

        public virtual List<string> Execute(Round currentRound)
        {
            var roundCategories = currentRound.Categories;

            List<string> categoryNames = new List<string>();
            foreach (Category category in roundCategories)
            {
                categoryNames.Add(category.CategoryName);
            }

            return categoryNames;
        }
    }
}