﻿using System;
using System.Threading.Tasks;

namespace Actions
{
    public class Login
    {
        private IWebLoginService _webLoginService;
        private IUserSessionRepo _userSessionRepository;

        public Login(IUserSessionRepo userSessionRepository, IWebLoginService webLoginService)
        {
            _webLoginService = webLoginService;
            _userSessionRepository = userSessionRepository;
        }

        public async Task<bool> Execute(string userID)
        {
            try
            {
                Player myPlayer = await _webLoginService.FindPlayerByID(userID);
                _userSessionRepository.CreateUserSession(myPlayer);
                return true;
            }
            catch (Exception e)
            {
                throw new Exception($"Error de Login: {e.Message}");
            }
        }
    }
}