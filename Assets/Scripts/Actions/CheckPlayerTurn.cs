﻿using System;
using System.Collections.Generic;

namespace Actions
{
    public class CheckPlayerTurn
    {
        private IGameSessionRepo _sessionRepo;
        private IUserSessionRepo _userSession;

        public CheckPlayerTurn(IGameSessionRepo sessionRepo, IUserSessionRepo userSession)
        {
            _sessionRepo = sessionRepo;
            _userSession = userSession;
        }

        public virtual bool Execute(int sessionID)
        {
            List<Answer> playerAnswers = new List<Answer>();
            List<Answer> opponentAnswers = new List<Answer>();
            GameSession currentGameSession = _sessionRepo.GetGameSessionByID(sessionID, false);

            try
            {
                if (_userSession.GetCurrentUserSession().Player.UserID == currentGameSession.Player1.UserID)
                {
                    playerAnswers = currentGameSession.MatchRounds[currentGameSession.CurrentRound.RoundNumber - 1].Player1Answers;
                    opponentAnswers = currentGameSession.MatchRounds[currentGameSession.CurrentRound.RoundNumber - 1].Player2Answers;
                }
                else
                {
                    playerAnswers = currentGameSession.MatchRounds[currentGameSession.CurrentRound.RoundNumber - 1].Player2Answers;
                    opponentAnswers = currentGameSession.MatchRounds[currentGameSession.CurrentRound.RoundNumber - 1].Player1Answers;
                }
            }
            catch (Exception e)
            {

                throw new Exception($"CheckPlayerTurn: {e.Message}");
            }

            return playerAnswers.Count <= opponentAnswers.Count;
        }

        public virtual bool Execute(GameSession gameSession)
        {
            List<Answer> playerAnswers = new List<Answer>();
            List<Answer> opponentAnswers = new List<Answer>();
            GameSession currentGameSession = gameSession;

            try
            {
                if (_userSession.GetCurrentUserSession().Player.UserID == currentGameSession.Player1.UserID)
                {
                    playerAnswers = currentGameSession.MatchRounds[currentGameSession.CurrentRound.RoundNumber - 1].Player1Answers;
                    opponentAnswers = currentGameSession.MatchRounds[currentGameSession.CurrentRound.RoundNumber - 1].Player2Answers;
                }
                else
                {
                    playerAnswers = currentGameSession.MatchRounds[currentGameSession.CurrentRound.RoundNumber - 1].Player2Answers;
                    opponentAnswers = currentGameSession.MatchRounds[currentGameSession.CurrentRound.RoundNumber - 1].Player1Answers;
                }
            }
            catch (Exception e)
            {

                throw new Exception($"CheckPlayerTurn: {e.Message}");
            }

            return playerAnswers.Count <= opponentAnswers.Count;
        }
    }
}