﻿using System.Threading.Tasks;

namespace Actions
{
    public class GetPlayerInfo
    {
        private IWebPlayerServices _webPlayerRepo;
        private IUserSessionRepo _userSessionRepo;
        public GetPlayerInfo(IWebPlayerServices webPlayerRepo, IUserSessionRepo userSessionRepo)
        {
            _webPlayerRepo = webPlayerRepo;
            _userSessionRepo = userSessionRepo;
        }

        public virtual async Task<Player> Execute()
        {
            return await _webPlayerRepo.FindPlayerByID(_userSessionRepo.GetCurrentUserSession().Player.UserID);
        }
    }
}