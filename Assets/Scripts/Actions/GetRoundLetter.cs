﻿namespace Actions
{
    public class GetRoundLetter
    {
        IGameSessionRepo _gameSessionRepo;

        public GetRoundLetter(IGameSessionRepo gameSessionRepo)
        {
            _gameSessionRepo = gameSessionRepo;
        }

        public char Execute()
        {
            return new GetCurrentRound(_gameSessionRepo).Execute().RoundLetter;
        }
    }
}