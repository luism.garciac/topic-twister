﻿namespace Actions
{
    public class GetSessionOpponent
    {
        private IGameSessionRepo _gameSessionRepository;
        private IUserSessionRepo _userSessionRepo;

        public GetSessionOpponent(IGameSessionRepo gameSessionRepository, IUserSessionRepo userSessionRepo)
        {
            _gameSessionRepository = gameSessionRepository;
            _userSessionRepo = userSessionRepo;
        }

        public Player Execute()
        {
            string playerID = _userSessionRepo.GetCurrentUserSession().Player.UserID;
            GameSession gameSession = _gameSessionRepository.GetCurrentGameSession();

            if (gameSession.Player1.UserID == playerID)
                return gameSession.Player2;
            else
                return gameSession.Player1;
        }
    }
}