﻿using System.Threading.Tasks;

namespace Actions
{
    public class FindMatch
    {
        private IWebMatchMakingServices _webMatchMakingServices;

        public FindMatch(IWebMatchMakingServices webMatchMakingServices)
        {
            _webMatchMakingServices = webMatchMakingServices;
        }

        public async Task<GameSession> Execute(Player clientPlayer)
        {
            GameSession newSession = await _webMatchMakingServices.FindOpponent(clientPlayer.UserID);

            return newSession;
        }
    }
}