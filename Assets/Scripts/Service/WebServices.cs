﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class WebServices
{
    private static readonly string _endPoint = "http://equipo06quark-001-site1.etempurl.com";
    //private static readonly string _loginServiceEndpoint = "https://login-topic-twister.herokuapp.com/";
    private static readonly string _loginServiceEndpoint = "http://localhost:8080";
    //private static readonly string _endPoint = "http://localhost:5026";

    private static IWebPlayerServices _webPlayerServices = new WebPlayerServices(_endPoint);
    private static IWebCategoryServices _webCategoryServices = new WebCategoryServices(_endPoint);
    private static IWebMatchMakingServices _webMatchMakingServices = new WebMatchMakingServices(_endPoint);
    private static IWebGameSessionServices _webGameSessionServices = new WebGameSessionServices(_endPoint);
    private static IWebLoginService _webLoginServices = new WebLoginServices(_loginServiceEndpoint);

    public static IWebPlayerServices GetWebPlayerServices()
    {
        return _webPlayerServices;
    }

    public static IWebCategoryServices GetCategoryServices()
    {
        return _webCategoryServices;
    }

    public static IWebMatchMakingServices GetMatchMakingServices()
    {
        return _webMatchMakingServices;
    }

    public static IWebGameSessionServices GetGameSessionServices()
    {
        return _webGameSessionServices;
    }

    public static IWebLoginService GetLoginServices()
    {
        return _webLoginServices;
    }
}
