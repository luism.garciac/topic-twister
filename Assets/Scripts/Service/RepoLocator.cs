﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class RepoLocator
{
    private static InMemoryCategoryRepo _inMemoryCategoryRepository = new InMemoryCategoryRepo();
    private static InMemoryPlayerRepo _inMemoryPlayerRepository = new InMemoryPlayerRepo();
    private static InMemoryRoundRepo _inMemoryRoundRepository = new InMemoryRoundRepo();
    private static InMemoryLetterRepo _inMemoryLetterRepository = new InMemoryLetterRepo();
    private static InMemoryGameSessionRepo _inMemoryGameSessionRepository = new InMemoryGameSessionRepo();
    private static InMemoryUserSessionRepo _inMemoryUserSessionRepository = new InMemoryUserSessionRepo();

    public static IUserSessionRepo GetUserSessionRepo()
    {
        return _inMemoryUserSessionRepository;
    }
    public static ICategoryRepo GetCategoryRepo()
    {
        return _inMemoryCategoryRepository;
    }

    public static IPlayerRepo GetPlayerRepo()
    {
        return _inMemoryPlayerRepository;
    }

    public static IRoundRepo GetRoundRepo()
    {
        return _inMemoryRoundRepository;
    }

    public static ILetterRepo GetLetterRepo()
    {
        return _inMemoryLetterRepository;
    }

    public static IGameSessionRepo GetGameSessionRepo()
    {
        return _inMemoryGameSessionRepository;
    }
}
