﻿using Newtonsoft.Json;
using System;
using UnityEngine;

public class JSonSerializationNewton : ISerializationOption
{
    public string ContentType => "application/json";

    public T Deserialize<T>(string text)
    {
        try
        {
            Debug.Log($"Raw JSON Data \n{text}");
            var result = JsonConvert.DeserializeObject<T>(text);
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogError($"Could not parse response {text}. {ex.Message}");
            return default;
        }
    }

    public string Serialize(object payload)
    {
        try
        {
            var result = JsonConvert.SerializeObject(payload);
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogError($"Could serialize payload {payload}. {ex.Message}");
            return default;
        }
    }
}
