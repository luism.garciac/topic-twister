﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public interface ISerializationOption
{
    string ContentType { get; }
    T Deserialize<T>(string text);
    string Serialize(object payload);

}
