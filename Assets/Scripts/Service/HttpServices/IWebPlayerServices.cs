﻿using System.Collections.Generic;
using System.Threading.Tasks;

public interface IWebPlayerServices
{
    Task<Player> FindPlayerByID(string ID);
    Task<bool> UpdatePlayerData(string playerId, PlayerData playerData);
    Task<List<Player>> FindPlayersLookingForMatch();

}