﻿using System.Collections.Generic;
using System.Threading.Tasks;

public interface IWebCategoryServices
{
    Task<List<Category>> GetRandomCategoryList();
    Task<char> GetRandomLetter();

}