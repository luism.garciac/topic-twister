﻿using System;
using System.Threading.Tasks;
using UnityEngine;

public class WebLoginServices : IWebLoginService
{
    private string _url;

    public WebLoginServices(string url)
    {
        _url = url;
    }

    public async Task<Player> FindPlayerByID(string playerID)
    {
        try
        {
            HttpClientService httpClientService = new HttpClientService(new JSonSerializationNewton());
            var url = $"{_url}/GetPlayerByID/{playerID}";

            Task<Player> getPlayerTask = httpClientService.Get<Player>(url);
            Player currentPlayer = await getPlayerTask;
            Debug.Log("Player Obtained");

            return currentPlayer;
        }
        catch (Exception e)
        {
            throw new Exception($"Error in FindPlayerByID: {e.Message}");
        }
    }

    public async Task<Player> FindPlayerByIDMongo(string playerID)
    {
        try
        {
            HttpClientService httpClientService = new HttpClientService(new JSonSerializationNewton());
            var url = $"{_url}/GetPlayerByIDMongo/{playerID}";

            Task<Player> getPlayerTask = httpClientService.Get<Player>(url);
            Player currentPlayer = await getPlayerTask;
            Debug.Log("Player Obtained");

            return currentPlayer;
        }
        catch (Exception e)
        {
            throw new Exception($"Error in FindPlayerByID: {e.Message}");
        }
    }
}

