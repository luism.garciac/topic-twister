﻿using System.Collections.Generic;
using System.Threading.Tasks;

public interface IWebGameSessionServices
{
    Task<List<GameSession>> FindOpenGameSessions(string playerID);
    Task<GameSession> GetSessionByID(int sessionID);
    Task<List<Answer>> UpdateAnswers(string userID, int sessionID, List<Answer> answers);
}