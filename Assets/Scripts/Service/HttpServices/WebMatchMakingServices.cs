﻿using System.Threading.Tasks;

public class WebMatchMakingServices : IWebMatchMakingServices
{
    private string _endpoint;
    public WebMatchMakingServices(string endpoint)
    {
        _endpoint = endpoint;
    }

    public async Task<GameSession> FindOpponent(string playerID)
    {
        HttpClientService httpClientService = new HttpClientService(new JSonSerializationNewton());
        var url = $"{_endpoint}/FindOpponent/id?playerID={playerID}";

        Task<GameSession> findOpponent = httpClientService.Get<GameSession>(url);
        GameSession newGameSession = await findOpponent;

        return newGameSession;
    }
}

