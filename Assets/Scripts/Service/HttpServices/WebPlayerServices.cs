﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class WebPlayerServices : IWebPlayerServices
{
    private List<Player> _playerList = new List<Player>();
    private string _endpoint;

    public WebPlayerServices(string endpoint)
    {
        _endpoint = endpoint;
    }

    public async Task<Player> FindPlayerByID(string playerID)
    {
        HttpClientService httpClientService = new HttpClientService(new JSonSerializationNewton());
        var url = $"{_endpoint}/GetPlayerById/id?id={playerID}";

        Task<Player> getPlayerTask = httpClientService.Get<Player>(url);
        Player currentPlayer = await getPlayerTask;
        Debug.Log("Player Obtained");

        return currentPlayer;
    }

    public async Task<Player> FindPlayerByIDMongo(string playerID)
    {
        HttpClientService httpClientService = new HttpClientService(new JSonSerializationNewton());
        var url = $"{_endpoint}/GetPlayerByIdMongo/id?id={playerID}";

        Task<Player> getPlayerTask = httpClientService.Get<Player>(url);
        Player currentPlayer = await getPlayerTask;
        Debug.Log("Player Obtained");

        return currentPlayer;
    } 

    public async Task<bool> UpdatePlayerData(string playerId, PlayerData playerData)
    {
        HttpClientService httpClientService = new HttpClientService(new JSonSerializationNewton());
        var url = $"{_endpoint}/UpdatePlayerData?id={playerId}";

        Task<bool> putPlayerData = httpClientService.Put<bool>(url, playerData);
        bool result = await putPlayerData;
        if (result)
            Debug.Log("Player Data Updated");
        else
            Debug.LogError("Could Not Update Player Data");

        return result;
    }

    public async Task<List<Player>> FindPlayersLookingForMatch()
    {
        HttpClientService httpClientService = new HttpClientService(new JSonSerializationNewton());
        var url = $"{_endpoint}/GetPlayersLookingForMatch";

        Task<List<Player>> getMatches = httpClientService.Get<List<Player>>(url);
        List<Player> playerMatches = await getMatches;
        Debug.Log("Matches Obtained");

        return playerMatches;
    }

    public async void GetAllPlayersAsync()
    {
        await GetAllPlayersTask();
    }

    public async Task GetAllPlayersTask()
    {
        HttpClientService httpClientService = new HttpClientService(new JSonSerializationNewton());
        var url = $"{_endpoint}/GetAllPlayers";

        Task<List<Player>> getPlayersTask = httpClientService.Get<List<Player>>(url);
        List<Player> playerList = await getPlayersTask;
        _playerList = playerList;

        RepoLocator.GetPlayerRepo().UpdateRepositoryData(playerList);
    }
}

