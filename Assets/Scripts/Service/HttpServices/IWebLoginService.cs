﻿using System.Threading.Tasks;

public interface IWebLoginService
{
    Task<Player> FindPlayerByID(string ID);
    Task<Player> FindPlayerByIDMongo(string ID);
}
