﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class WebGameSessionServices : IWebGameSessionServices
{
    private List<GameSession> _gameSessions = new List<GameSession>();
    private string _endpoint;

    public WebGameSessionServices(string endpoint)
    {
        _endpoint = endpoint;
    }

    public async Task<List<GameSession>> FindOpenGameSessions(string playerID)
    {
        HttpClientService httpClientService = new HttpClientService(new JSonSerializationNewton());
        var url = $"{_endpoint}/FindOpenGameSessions/id?id={playerID}";

        Task<List<GameSession>> getGameSessions = httpClientService.Get<List<GameSession>>(url);
        List<GameSession> gameSessions = await getGameSessions;
        Debug.Log("Game Sessions Obtained");

        return gameSessions;
    }

    public async Task<GameSession> GetSessionByID(int sessionID)
    {
        HttpClientService httpClientService = new HttpClientService(new JSonSerializationNewton());
        var url = $"{_endpoint}/GetGameSessionById/id?id={sessionID}";///GetGameSessionById/id?id=4

        Task<GameSession> getGameSession = httpClientService.Get<GameSession>(url);
        GameSession gameSession = await getGameSession;
        Debug.Log($"Game Session id {sessionID} Obtained");

        return gameSession;
    }

    public async Task<List<Answer>> UpdateAnswers(string userID, int sessionID, List<Answer> answers)
    {
        HttpClientService httpClientService = new HttpClientService(new JSonSerializationNewton());
        var url = $"{_endpoint}/UpdateAnswers?userID={userID}&sessionID={sessionID}";

        Task<List<Answer>> validateAnswers = httpClientService.Put<List<Answer>>(url, answers);
        List<Answer> result = await validateAnswers;

        if (result.Count != 0)
            Debug.Log("Answers Updated");
        else
            Debug.LogError("Could Not Update Answers");

        return result;
    }
}

