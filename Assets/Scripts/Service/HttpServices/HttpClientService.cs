﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

public class HttpClientService
{
    private readonly ISerializationOption _serializationOption;

    public HttpClientService(ISerializationOption serializationOption)
    {
        _serializationOption = serializationOption;
    }

    public async Task<TResultType> Get<TResultType>(string url)
    {
        try
        {
            UnityWebRequest web = UnityWebRequest.Get(url);
            web.SetRequestHeader("Content-Type", _serializationOption.ContentType);

            var operation = web.SendWebRequest();

            while (!operation.isDone)
            {
                await Task.Yield();
            }

            if (web.result != UnityWebRequest.Result.Success)
            {
                throw new Exception("WebPlayerRepository Error in GET request = " + web.error);
            }

            var result = _serializationOption.Deserialize<TResultType>(web.downloadHandler.text);

            var serializedData = _serializationOption.Serialize(result);

            return result;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public async Task<TResultType> Put<TResultType>(string url, object payload)
    {
        try
        {
            var serializedPayload = _serializationOption.Serialize(payload);

            UnityWebRequest web = UnityWebRequest.Put(url, serializedPayload.Trim());
 
            web.SetRequestHeader("Content-Type", _serializationOption.ContentType);

            var operation = web.SendWebRequest();

            while (!operation.isDone)
            {
                await Task.Yield();
            }

            if (web.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError("WebPlayerRepository Error in PUT request = " + web.error);
            }

            var result = _serializationOption.Deserialize<TResultType>(web.downloadHandler.text);
            var serializedData = _serializationOption.Serialize(result);

            return result;
        }
        catch (Exception ex)
        {
            Debug.LogError($"{nameof(Get)} failed: {ex.Message}");
            return default;
        }
    }
}
