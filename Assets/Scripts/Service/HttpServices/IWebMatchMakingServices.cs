﻿using System.Threading.Tasks;

public interface IWebMatchMakingServices
{
    Task<GameSession> FindOpponent(string playerID);
}