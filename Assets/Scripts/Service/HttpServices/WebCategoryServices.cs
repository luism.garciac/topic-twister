﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class WebCategoryServices : IWebCategoryServices
{
    private string _endpoint;

    public WebCategoryServices(string endpoint)
    {
        _endpoint = endpoint;
    }

    public async Task<List<Category>> GetRandomCategoryList()
    {
        HttpClientService httpClientService = new HttpClientService(new JSonSerializationNewton());
        var url = $"{_endpoint}/GetRandomCategories";

        Task<List<Category>> getCategories = httpClientService.Get<List<Category>>(url);
        List<Category> categories = await getCategories;
        Debug.Log("Categories Obtained");

        return categories;
    }

    public async Task<char> GetRandomLetter()
    {
        HttpClientService httpClientService = new HttpClientService(new JSonSerializationNewton());
        var url = $"{_endpoint}/GetRandomLetter";

        Task<char> getLetter = httpClientService.Get<char>(url);
        char letter = await getLetter;
        Debug.Log("Letter Obtained");

        return letter;
    }
}

