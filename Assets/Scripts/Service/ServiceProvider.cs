﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ServiceProvider
{

    // ELIMINAR ESTA CLASE NO SE USA
    private static UserSession _topicTwisterGame;

    public static UserSession CreateTopicTwisterGame(Player myPlayer)
    {
        _topicTwisterGame = new UserSession(myPlayer);
        return _topicTwisterGame;
    }

    public static UserSession GetTopicTwisterGame()
    {
        return _topicTwisterGame;
    }
}
